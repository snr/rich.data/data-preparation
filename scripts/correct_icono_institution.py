import typing as t
import argparse
import csv
import re
import os

# ********************************************************
# correct a column of institution names, replacing
# invalid ones with valid ones, and write the output
# to file.
# ********************************************************

# { <invalid institution name>: <valid institution name> }
# to build `mapper`, run `main.py`, which will run `src/verifier.py`, which will produce
# a list of invalid institution names in `out/verifier`.
mapper = { "Musée \nCarnavalet"                           : "Musée Carnavalet (Paris Musées)"
         , "Bibliothèque historique de la Ville de Paris" : "Bibliothèque historique de la Ville de Paris (Bibliothèques spécialisées de la Ville de Paris)"
         , "Bibliothèque de l'INHA"                       : "Institut national d'histoire de l'art"
         , "Bibliothèque Forney, Paris"                   : "Bibliothèque Forney (Bibliothèques spécialisées de la Ville de Paris)"
         , " Bibliothèque de l'Hôtel de Ville "           : "Bibliothèque de l'Hôtel de Ville (Bibliothèques spécialisées de la Ville de Paris)"
         , " Bibliothèque Marguerite Durand "             : "Bibliothèque Marguerite Durand"
         , " Bibliothèque Forney "                        : "Bibliothèque Forney (Bibliothèques spécialisées de la Ville de Paris)"
         , "Bibliothèque de l'Hôtel de Ville\n"           : "Bibliothèque de l'Hôtel de Ville (Bibliothèques spécialisées de la Ville de Paris)"
         , "Bibliothèque Forney\n"                        : "Bibliothèque Forney (Bibliothèques spécialisées de la Ville de Paris)"
         , "Bibliothèque Forney"                          : "Bibliothèque Forney (Bibliothèques spécialisées de la Ville de Paris)"
         , "Musée Carnavalet"                             : "Musée Carnavalet (Paris Musées)"
         , "Maison de Victor Hugo "                       : "Maison Victor Hugo (Paris Musées)"
         , "Palais Galliera"                              : "Palais Galliera (Paris Musées)"
         , "Petit Palais"                                 : "Petit Palais. Musée des Beaux-Arts de la Ville de Paris (Paris Musées)"
         , "Musée Bourdelle"                              : "Musée Bourdelle (Paris Musées)"
         , "Maison de Victor Hugo"                        : "Maison Victor Hugo (Paris Musées)"
         , "Musée de la Maison de Balzac"                 : "Maison de Balzac (Paris Musées)"
         , "\nPetit Palais"                               : "Petit Palais. Musée des Beaux-Arts de la Ville de Paris (Paris Musées)"
         , "Musée Carnavalet "                            : "Musée Carnavalet (Paris Musées)"
         , "Musée de la Maison Balzac"                    : "Maison de Balzac (Paris Musées)"
         , "Bibliothèque municipale de Rouen"             : "Bibliothèque municipale et partimoniale Villon"
         , "Bibliothèque Marguerite Durand"               : "Bibliothèque Marguerite Durand (Bibliothèques spécialisées de la Ville de Paris)"
         , "Bibliothèque de l'Hôtel de Ville de Paris"    : "Bibliothèque de l'Hôtel de Ville (Bibliothèques spécialisées de la Ville de Paris)"
         , "Médiathèque musicale de Paris"                : "Médiathèque musicale de Paris (Bibliothèques spécialisées de la Ville de Paris)"
         , "Bibliothèque des Littératures Policières"     : "Bibliothèque des littératures policières (Bibliothèques spécialisées de la Ville de Paris)"
         , "Bibliothèque du Tourisme et des Voyages"      : "Bibliothèque du tourisme et des voyages - Germaine Tillion (Bibliothèques spécialisées de la Ville de Paris)"
         , "Musée de la Libération de Paris - musée du Général Leclerc - musée Jean Moulin" : "Musée de la Libération Leclerc Moulin (Paris Musées)"
         }


def reader(input_filename:str) -> t.List[t.List[str]]:
    """
    read the file at `input_filename`. we need to use `csv`
    because the file may contain line jumps within a single cell

    :returns: list with one item per row. each item is a list of strings, with one item per column.
    """
    try:
        with open(input_filename, mode="r", newline="") as fh:
            col = csv.reader(fh, delimiter="\t", quotechar="\"") # fh.read()
            col = [ c for c in col ]

        assert all(len(c)==1 or len(c)==0 for c in col), "input file contains more than 1 column !"

    except FileNotFoundError:
        print(f"\n>>> file `{input_filename}` not found. exiting...\n")
        exit(1)

    return col


def replacer(col: t.List[t.List[str]]) -> t.List[t.List[str]]:
    """
    replace invalid values in `col` by the correct institutions
    """
    out = []
    for c in col:
        if len(c):
            try:
                out.append([ mapper[c[0]] ])  # c's len is always 1, so we recreate a one item list before adding it to out.
            except KeyError:
                out.append(c)  # c[0] is not in `mapper.keys()` so it should be correct => append it to `out`
        else:
            out.append(c)
    # ensure there's no data loss
    assert all( len(col[i]) == len(o) for i,o in enumerate(out) ), "probable data loss : number of cells in output differs from the input"
    assert len(col) == len(out), "probable data loss : number of rows in output differ from the input"
    return out


def writer(col:t.List[t.List[str]], input_filename:str) -> None:
    """
    write `col` to file. the filename is the basename of the input file + "_corrected.csv"
    """
    basename = os.path.splitext(os.path.basename(input_filename))[0]
    outname = f"{basename}_corrected.csv"
    with open(outname, mode="w", newline="") as fh:
        writer = csv.writer(fh, quotechar="\"", delimiter="\t")
        writer.writerows(col)
    print(f"\n>>> output written to `{outname}`\n")
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser( prog="correct_icono_institution"
                                    , description="correct the `institution` column in the input CSV so that it only contains valid values and write to a new file.")
    parser.add_argument("filename", help="the name of the file to process. should be a 1 column CSV with `\t` separator and no header.")

    args = parser.parse_args()

    # pipeline
    col = reader(args.filename)
    col = replacer(col)
    writer(col, args.filename)
