# *********************************************************
# prepare the images in the iconography directory.
#
# let's say you have downloaded all the folders that
# contain iconography images, but those folders have an
# arbirary structure (subfolders...), non-image files
# and the names are messy (they have spaces...).
#
# this is a CLI that takes that folder as an input,
# flattens it, keeps only image files and renames them
# by replacing spaces to `_`. the output is in
# a new folder, and the input stays untouched.
# *********************************************************

from PIL import Image, UnidentifiedImageError
from tqdm import tqdm
import typing as t
import argparse
import shutil
import re
import os


def check_dirs(indir:str):
    """
    create the output directory `outdir` and translate directories into abspaths.
    """
    indir = os.path.abspath(indir)
    outdir = f"{indir}_prepared"
    outdir = os.path.join( os.path.split(indir)[0]
                         , f"{os.path.split(indir)[1]}_prepared" )

    if not os.path.exists(indir):
        raise FileNotFoundError(f"input directory `{indir}` not found ! exiting...")

    if os.path.exists(outdir) and len(os.listdir(outdir)):
        raise FileExistsError(f"output directory {outdir} is not empty ! please delete it before restarting.")
        exit(1)

    if os.path.exists(outdir):  # exists but empty
        os.rmdir(outdir)

    os.makedirs(outdir)

    return indir, outdir


def verify_image(filename:str) -> bool:
    """verify that the image at `filename` is valid"""
    Image.MAX_IMAGE_PIXELS = 10**10  # avoid DecompressionBombError
    out = False
    try:
        with Image.open(filename) as img:
           img.verify()  # throws an exception if there's an error
           out = True    # if all is good till here, it's a valid image
    except OSError:      # the image couldn't be opened
        # print("file is not an image :", os.path.basename(filename))
        pass
    return out


def clean_filename(filename:str) -> str:
   """clean a filename"""
   return re.sub(r"\s+", "_", filename).strip()


def prepare(indir:str, outdir:str) -> None:
    """
    main function:
    > find all image files in `indir` recursively,
    > clean their name
    > copy them to `outdir`
    """
    tomove = {}  # { <path to input file with dirty filename>: <path to output file with clean filename> }, containing only image files

    # select all images, clean their name, add them to `tomove`
    for root,dirs,files in os.walk(indir):
        for fn in files:
            if verify_image(os.path.join(root, fn)):                     # assert that it's a valid image
                tomove[os.path.join(root, fn)] = os.path.join( outdir, clean_filename(fn) )  # clean the name

    # copy everything to the output
    for k,v in tqdm(tomove.items(), desc="copying image files to output"):
        shutil.copy2(k,v)

    print(f"\n>>> cleaned files copied to {outdir}")
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser( prog="prepare_icono_dir"
                                    , description="prepare the directory containing the iconography images."
                                                 +"from a directory with an arbitrary structure (subfolders), "
                                                 +"containing non-image files and filenames with spaces, "
                                                 + "create a flat directory of images only, with normalized names")
    parser.add_argument("indir", help="input directory")

    args = parser.parse_args()

    indir, outdir = check_dirs(args.indir)

    prepare(indir, outdir)
