import shutil
import os


# ****************************************************************
# flatten the current directory to an output directory
# `./_flattened/`.
# all files in subdirectories of the current directory are
# copied to `./_flattened`, except for the files in `./_flattened`
#
# usage:
# python flatten.py
# python3 flatten.py
# ****************************************************************



curdir  = os.path.dirname(os.path.abspath(__file__))
dest    = os.path.join(curdir, "_flattened")
flatten = []

if not os.path.exists(dest):
  os.makedirs(dest)

for root,dirs,files in os.walk(curdir):
  if not root == dest:  # don't process the `dest` directory
    for fn in files:
      flatten.append( os.path.join(root,fn) )
   
for f in flatten:
  shutil.copy2(f, dest)

