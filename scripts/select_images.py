import random
import shutil
import os

# *********************************************
# select 120 files from the current directory
# and move them to `./out/` (done to select 
# random images
# *********************************************

files = [ f for f in os.listdir() if f != os.path.basename(__file__) ]

if not os.path.isdir("./out"):
  os.makedirs("./out")

nums = [ random.randint(0, len(files)-1) for _ in range(120) ]

for n in nums:
  shutil.copy2( files[n], os.path.join("./out", files[n]) )


