import argparse
import re
import os

# *****************************************************
# transform a pipe-separated column (csv or txt format,
# 1 column only,  no header/column title)
# into a list of deduplicated values
# *****************************************************


def reader(input_filename:str) -> str:
    """read the file at `input_filename`"""
    try:
        with open(input_filename, mode="r") as fh:
            col = fh.read()
    except FileNotFoundError:
        print(f"\n>>> file `{input_filename}` not found. exiting...\n")
        exit(1)
    return col


def clean_col(col, lowercase:bool=False) -> str:
    """clean the column `col`"""
    # simplify col
    col = col.lower() if lowercase else col
    col = re.sub(r"[ \t]+", " ", col)
    col = re.sub(r"\s*\|\s*", "\n", col)
    col = col.split("\n")
    # deduplicate and sort col
    col = list(set([ c.strip() for c in col if not re.search(r"^\s*$", c) ]))
    col.sort()
    # convert back to string
    col = "\n".join(c for c in col)
    return col


def writer(col:str, input_filename:str) -> None:
    """
    write `col` to file. the filename is the basename of the input file + "_deduplicated.txt"
    """
    basename = os.path.splitext(os.path.basename(input_filename))[0]
    outname = f"{basename}_deduplicated.txt"
    with open(outname, mode="w") as fh:
        fh.write(col)
    print(f"\n>>> output written to `{outname}`\n")
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser( prog="pipedcol_to_deduplicated"
                                    , description="transform a pipe-separated column (csv or txt format, "
                                                 +"1 column only,  no header/column title) "
                                                 +"into a list of deduplicated values")
    parser.add_argument("filename")
    parser.add_argument("-l", "--lowercase", action="store_true", help="convert all text to lowercase")

    args = parser.parse_args()

    # pipeline
    col = reader(args.filename)
    col = clean_col(col, args.lowercase)
    writer(col, args.filename)



