"""
some files may have been by error moved to `cleanup/iconography`.
in this case, move them back to out/iconography, and if necessary,
update `log_iconography_outfile.json`.

this script must be run from the root of `1_Iconography`, and not
from the `script` dir.

the variable `files` can be updated with the list of filenames printed
by `Iconography.images_not_in_out()`.
"""

import shutil
import json
import re
import os

# basic variables
curdir     = os.path.dirname(os.path.abspath(__file__))
cleanupdir = os.path.join(curdir, "cleanup", "iconography")
outdir     = os.path.join(curdir, "out", "iconography", "images")
fp_logdl   = os.path.join(curdir, "src", "logs", "log_iconography_outfile.json")
fp_logct   = os.path.join(curdir, "src", "logs", "log_iconography_compressthumbnail.json")
with open(fp_logdl, mode="r") as fh:
    logdl = json.load(fh)
with open(fp_logct, mode="r") as fh:
    logct = json.load(fh)

# files that are in `logdl` but not in `outdir`
files        = ['qr134b8784d2ace436ea879fe8337be03fc.JPG', 'qr15b0e2d689a5749b88bc51fe1f7e97ba5.JPG', 'qr1c3b21f1a75714b8a98bddc6ea4f020d6.JPG', 'qr16422e85d8acc4869aa775bc300dd2903.JPG', 'qr112bb196aaac5405297d082cb3e7ba12e.JPG', 'qr1ce2f196c167f4b04bd7068f548223d8b.JPG', 'qr16d73b297c69f4e239f92c789857788d0.JPG', 'qr14d1bd5b693204e52a1021d1b1caf9fcc.PNG', 'qr147f8a2937ecc45eda7b0a0e381b903ed.PNG', 'qr1532cda2d35f54a0bb0f1e543d08997e4.PNG', 'qr14fb78a7405444466ae93af73058854e2.PNG', 'qr1a507314ef690493794b007624e021852.PNG', 'qr13984426cd0904f588737d3392dd82b5e.PNG', 'qr1c37f1115dcf344709cdf8a1e82f7b009.PNG', 'qr10080977273d145d4af7673d416082736.PNG', 'qr1bd3ddf7b8cee44b28a71261b55c08251.PNG', 'qr14d2ea48870e14480b6ef5ecbc0cd930d.PNG', 'qr12efd9ac05582479199dd374acca8a549.PNG', 'qr1556db734246645c48ecce2b23ca2aa61.PNG', 'qr13e5a15171c734535837a61e371556b5c.PNG', 'qr112a02e5c6dd747c5b8dd63d710d86214.PNG', 'qr1d11910f3e67d4bc4a6ac28864eb00b0b.PNG', 'qr15ae61268a5364789b322bd8b79a1c5ff.PNG', 'qr172d49d0e3f074aa3bc6946bff62938e7.PNG', 'qr1951bba90ae2c4fc5a1169809cc4e30b5.PNG', 'qr118557eae89484ff9860cc2f66797ee9e.PNG', 'qr1501c3765c3cc4294867e7b412dac6dad.PNG', 'qr153c5eee239054244b00014722b4a8df2.PNG', 'qr1c53d947f5ff143fa96fd8fd7845b2bfd.PNG', 'qr1ef6bd34818c74f89a93dfd9165c5af1d.PNG', 'qr1e3be8193f58f4ba2868bba919ac8f4c5.PNG', 'qr102d86d6696c64f4aad5a947c01403cce.PNG', 'qr13ac80a4e010a42debd32796d41af916f.PNG', 'qr1fe1574b6ac074b92825584b24c6c3124.PNG', 'qr1581cfd2bfb3d46e28740df6019047879.PNG', 'qr1d9edc113dc634503aa266bb9ad369011.PNG', 'qr115f615e6865a42ceb9085cd11a70486f.PNG', 'qr1be7bcd80636643b9a7520dd9f117874b.PNG', 'qr1041ac762e64f4dc482f060cbfadf350f.PNG', 'qr1438eb3763a384b17a799497e47bbf12e.PNG', 'qr1eeba059d22f448bb8863af2982d27910.PNG', 'qr1383b9dae5970413da597d4e72c994f56.PNG', 'qr1defb92ed7b4d4c45a92b547038cefee8.PNG', 'qr1ff3b4d691b2d44d2b388e30ac089c5c4.PNG', 'qr179751a64f55b479cbeb2f3cc6ade06fa.PNG', 'qr174c20bfec0364b56baca363bdca8c545.PNG', 'qr19a483e1278574abaafc1bc49edfcdedb.PNG', 'qr1368b4de6ecdd49738963a9f3f8c9153e.PNG', 'qr1dbaa35df702546c099c89dcd251ed6b4.PNG', 'qr14a9ce67b66a54d0a846cb86b7237bc39.PNG', 'qr19918e7873afc40bb8d00c2ca1cf714fb.PNG', 'qr1c1967ffd830b46ca8437cd90316e5bfe.PNG', 'qr1143f8e2702804acdb60199532c80ba9a.PNG', 'qr167265706d5aa4751b4b75a9f9facbce9.PNG', 'qr13527de2442a24349a728bac7eef1f3ec.PNG', 'qr175a622855b494317a1c163ffa9fb31cb.PNG', 'qr1651fd039daf843ab98df9d1190b1a6d7.PNG', 'qr1ca45c0940cbc49a996168e9ee5a795d0.PNG', 'qr1a83be36c352c4e4b933594af4c1d3c41.PNG', 'qr117e1a599b19d4fa585736cee6b9ba761.PNG', 'qr19292a091215242ffad771ea878480413.PNG', 'qr1420041f8b3ad4c088a9a64c7d39e9471.PNG', 'qr1b22035b2f8114426bc7582dacaf4c3cd.PNG', 'qr105796494a03d4df5b1afabbf7deb67f4.PNG', 'qr15395ed7632d4489ca20a1b3639e94a5b.PNG', 'qr107ae19fb43dc432fb5afb84fbb23617d.PNG', 'qr1eb9cd1dff92e40b68fe982f760c8187b.PNG', 'qr1330b6e8997074ef2b7655f11e3abc6eb.PNG', 'qr15a6350ad75664713b78de18de1a5974d.PNG', 'qr1e93cf7a7be2e4d2aa701b355914c0a98.PNG', 'qr19155fc0ea7e041ff9933878a132ac493.PNG', 'qr1ef268ddd9e1044bbbca4a212589b6b24.PNG', 'qr1329ddf8b1e324860a9a5785ed0970988.PNG', 'qr1bda8ee2309bc46d5808006ef2e14cc61.PNG', 'qr1ed77db43ec3f40cf81752202ab3182a5.PNG', 'qr1feb71bf11b784554b825f19e5d60529a.PNG', 'qr18acea4d1418f406d8973579104f83b71.PNG', 'qr1d60a993d31fb43c28f3fc57ce0e95d3c.PNG', 'qr1a0750951357244429c446928c9960dac.PNG', 'qr123e5c5d518ef4bab99d580e15eecdc09.PNG', 'qr1ff1a23e4eead466c8a89ceee25c19de5.PNG', 'qr1cd66f6acfd0b424a9169039fcbe8d163.PNG', 'qr1838fbe22b0d341fd9c62a9a376efeb67.PNG', 'qr17cf1d8cc050448fc9f807dcb892038d2.PNG', 'qr1cc03e369473f4b3da1d7ca221cce6606.PNG', 'qr188dad434c56a4948bd70204b49b3602c.PNG', 'qr1d3f59d08e27348fab4d96085bed29e3d.PNG', 'qr111103fa71da8476aa92d97158daefc93.PNG', 'qr1d441325413744cd9bb456f0def050bd3.PNG', 'qr163c0fed2e65244409638c8ea1ac25b3c.PNG', 'qr14a3d88913db34acb83590b419228e480.PNG', 'qr1549342c667d84f2fac80210975c94383.PNG', 'qr1356cbd1a24f34a56aec6c0e8b5a70825.PNG', 'qr1ea9928b3750846549201e30f6f3a65ab.PNG', 'qr14529a604e58c4f378a08619de0694909.PNG', 'qr13c7d2ad5f82244469f81a4c51952113c.PNG', 'qr118f3239de1d24d4c95ab68c6bfc1b0a3.PNG', 'qr15b8ab1fcf2484fee8ed7cdc2694d2203.PNG', 'qr1afcc85e098a14d05aa86b37bdd253107.PNG', 'qr1fa3d7c6be60a4311a03d6268851951bb.PNG', 'qr1addcec478c1d4463b4ccc21343308b78.PNG', 'qr1d027fbec50e547c18f62a158e4949c88.PNG', 'qr134c694a696f9453283a0e54456473da0.PNG', 'qr16635af3490e946bb973d38b61958fbcb.PNG', 'qr1c59e90fd03ab4c3499ea21a124435124.PNG', 'qr188ed16c48f8a4d72bdfb31f5043cfa97.PNG', 'qr1baa6291b9ef64830bbe13457d7f504dd.PNG', 'qr14133b1a7b7cb4be5965a63f991f1278d.PNG', 'qr1c7270c54d6084d2ba6bf8653da244499.PNG', 'qr1861bbc4a4eda45cf8dee83115b574780.PNG', 'qr1698982d381704cfba719663b57679365.PNG', 'qr19e0c1186c10341a1800548191be877c9.PNG', 'qr160a6b18a110f4d3b95637676ebd1af8a.PNG', 'qr15b7f04a7a44e49a1bee4d66394970547.PNG', 'qr142389ce38e32445d91bd506837b3b1cd.PNG', 'qr12dca55a43a5447528351092d736c4e3f.PNG', 'qr1a56dab77155743ecb9780d4ced138209.PNG', 'qr15a588748afef4213a94b3b4e4800c2e9.PNG', 'qr1e3cc9a0005744eeba0d03ac770992855.PNG', 'qr1125bd2029ab446449015a44e15176ead.PNG', 'qr1e3230cd910764d4a8e0f5ff1fc8943c7.PNG', 'qr126a7e5f2a1cd4acda82369fae36b0911.PNG', 'qr1d0d82434b3d24761b7ef069427682570.PNG', 'qr1564f34d7880547559b90962c95eed8fb.PNG', 'qr1eae41d28ca7c4cab8f4606277057b640.PNG', 'qr1bf33eb35729e4529b15c3e0ffa09a5e5.PNG', 'qr1a299937a96ae4baaab907116b2a05acd.PNG', 'qr1d7c54cfa9fc74a83acc9b70b8e6f1967.PNG', 'qr1bd9596983c894d229e2e855758949821.PNG', 'qr10f8b362bd9bc4275b8ef7020841ce5de.PNG', 'qr1c32b55d161ec47f089cdf96252fd658a.PNG', 'qr1536a219810834fe5ae00d59b429f8e96.PNG', 'qr16542726f8f73458a892ccb9f6a5d1fe4.PNG', 'qr1342347ad0eac432eb6d745028f8bc208.PNG', 'qr15aa99b64ab3a4521ab9790d40364bf61.PNG', 'qr1d4ddcb8f8589426e913118c89ab5ccdc.PNG', 'qr1f1024685e8c1499e850d936520c41c9c.PNG', 'qr174cc095d303d4642802bfaf42b5f8c29.PNG', 'qr1a9fab2df62be445e9c168d752647572e.PNG', 'qr1c5e700e0eeba4965bf0ea0b7d3c598dd.PNG', 'qr1f558726bc754462ea14e6fb32555501d.PNG', 'qr1d463e503f9ad4762add97ce72de9f885.PNG', 'qr1bc63efb742b24ed095cba23cfb641228.PNG', 'qr10b0e3e8a4c304cbb8a24a8c3d7360e6c.PNG', 'qr1f3384b7823cf4d90ae120db133824d77.PNG', 'qr1dd19cf81935b42fa85445ce11b422f4e.PNG', 'qr15fd33c4c06224f5fa44bdecdd8ef98ce.PNG', 'qr1f7bc5f643b5e4fde8522248a240e23ab.PNG', 'qr130243a38c53a45b1b73994f9a71f133e.PNG', 'qr12e39f1a8fd824a54b336461abedbf002.PNG', 'qr1c45ff5a658954066a3e5ed2fd1df6629.PNG', 'qr1f12826f29382462fa50e86d6f47019a2.PNG', 'qr162790bdfd5184466bf5c08929d033912.PNG', 'qr18d9827f0672b45829186b506f4255add.PNG', 'qr1a56b9c4e9a224c569d8a58ff6dea3eb5.PNG', 'qr1b22e2f63c9e1435db83e35cc4683905a.PNG', 'qr1b9f39bbef70a4aeab3901aabdf4c6c97.PNG', 'qr162804599b32b4723b4befff2132e4bcd.PNG', 'qr1b0757235749341ca82dd1d9b20d64102.PNG', 'qr17a7ffabda0f4442a8763620bffb3a94f.PNG', 'qr1d19510b7daa2470fa241295e37d4d643.PNG', 'qr1ca2871c68dc74351837a88a53f8c554d.PNG', 'qr1ffacf904973048089e16128539bcdad9.PNG', 'qr1130c78924ece4f20826a3c545c7de54e.PNG', 'qr1df0b3d4586dd44119d83db1318f4ef59.PNG', 'qr1fbbebb1558c54a84a88b236456ff8639.PNG', 'qr183cd7878f1ff48cdaf15584b24de3b33.PNG', 'qr185749dd6080144668b3d555b4e4b5da5.PNG', 'qr135562c3f913441cdbb17098b67295ed8.PNG', 'qr151990a0f0ea04962919cf6d3d28eaff9.PNG', 'qr1f72b3dbc4f09419a8f3e86288d17f523.PNG', 'qr11f826b9b4b5542e48408cc4c5670e417.PNG', 'qr109fa9cc1d58d4fedaca18f6f66c01d98.PNG', 'qr1f73589ee5018402ea7fe7d0441121c24.PNG', 'qr1773aaf076460479780212d19c7aa97d3.PNG', 'qr110afdb369cd04ce49cd4bd9d575dfa4a.PNG', 'qr18eb59e5b9d5944bfaa02fc65a63061f7.PNG', 'qr1bc29971f8c7847739070b631de05e574.PNG', 'qr184da129f8fdb49898a72e2f5d035dd49.PNG', 'qr1f7f70c17d7e34c7a8cd037ab32d249ae.PNG', 'qr1183ffbcd40b249e59ec1d867b779cad4.PNG', 'qr122ea74358fde48e2be660efeb4963714.PNG', 'qr1d4280fb8d3774da9b27d7360f2c97b2d.PNG', 'qr1002cda6702ab4c0eb710d3c012970aa7.PNG', 'qr1a3c4a8fbed644fd6a9a2236283dc5b99.PNG', 'qr11864482789dc4eedb3ab8b312a354ea5.PNG', 'qr1e2c5dfa1488442f4b4e4001953a18917.PNG', 'qr151f29f34e45b4d70b45e350757d01278.PNG', 'qr19f861ff8e0634aed8022052840b8bcdd.PNG', 'qr15ef8faed8c1348729b65dc85bc0033a0.PNG', 'qr1a35251f4550e453895b0d7da90f93128.PNG', 'qr1e9b335650465468d978c16a6aac43b80.PNG', 'qr1236a08b6236c423093c9760e0ac35738.PNG', 'qr136d2a420d28d4e31baf01923dfa1d3e4.PNG', 'qr1b54cb2a95045459c97c2dd88952c29a4.PNG', 'qr174ba96e4b89c44ad831ee26d38535706.PNG', 'qr127801deb86d648bb865e0948ddc784ed.PNG', 'qr1057a6a4324644422801a11701ce4df18.PNG', 'qr1299dc1263e6945dc9a173a1555beab0c.PNG', 'qr1c548fdd977664ef89c6fb6b4a02fb737.PNG', 'qr12f93dd9da0094da4ac4bc2a8b1882dca.PNG', 'qr19e69e3553dc5451ba6ae13929234d820.PNG', 'qr10869547df41440d9a06a380a6ea4e2ec.PNG', 'qr1ac5bc50a364941719c10009a9498d18f.PNG', 'qr15c807cd1c297416aa4a00a3ee9d51b31.PNG', 'qr11101f75a4168497fa5135f74e166bff7.PNG', 'qr17b18e8dcdd1e4fbb8d103d25c2f311f4.PNG', 'qr1113faae1c20c48539bf6ccd37f19a531.PNG', 'qr1f231a0f62cd14d9390607174289ef135.PNG', 'qr130467053e7054b1babc88fbef4ecae2c.PNG', 'qr142860ec37f8d457bbb4ca9a727c07bc2.PNG', 'qr16915114cbc1c4f2daec2bf775d3a3e94.PNG', 'qr15c3855ae06d643df8ab0314726508252.JPG', 'qr1a23c3c8b85ca4e2f8f8a5dbd69461434.JPG', 'qr190cf3ee266384d1b817342f3970174d8.JPG', 'qr1b71a00eb3e1745cc89829325c2b7585b.JPG', 'qr19301f453f9184ebf93f3ea2cc7233b6c.JPG', 'qr122e73dd550c44576ae819ed2846d9204.JPG', 'qr1a44faaf9da77455a8c1c80a3beecb2f5.JPG', 'qr1c3030e58db894adfacf75047b8ea4839.JPG', 'qr15e76c024b43e4039b9a9e405753991d5.JPG', 'qr167259ef393334645985dae71e061313f.JPG', 'qr1db9780a04976419f94e66ac8483a51fd.JPG', 'qr1d5b7cdd03f434d34b606a22134def637.JPG', 'qr1db8177133ebf4cff83ca4e9618f22cbf.JPG', 'qr192e1b9539b2641e186d8d3ef00247a15.JPG', 'qr167822f90a71a47baa8d451fc773c2936.JPG', 'qr12514d40f367e4bb5971772833d7d0d99.JPG', 'qr13fa461123fc64e41a3b420e9384d26c3.JPG', 'qr18ec1d22f0d004084870250a81366aa3c.JPG', 'qr140ee193c40d44acc8ab45802ce07c778.JPG', 'qr1d1a8ba0a558f4f4d9eb9919285cd82d3.JPG', 'qr1ea93be4862764294a2ce825f3322fa14.JPG', 'qr1bb609981405d455f81c26a1c34402531.JPG', 'qr1ff12b3787f614941a7397a5a22cc5e67.JPG', 'qr13b4b6ea92aa74787affb0c778c47f554.JPG', 'qr1647f96c7d5124e26bf02213989eeed92.JPG', 'qr1209014e03060429f9ac7a5634ea40ad2.JPG', 'qr159d7e11038724878a11a760d0dc426b1.JPG', 'qr19fd0d07c92364316a07913dd1659a898.JPG', 'qr18ca5b2aa662f4021b7f4c72d2fb19088.JPG', 'qr1b0321e84200d4a18928b541ba20ffdf8.JPG', 'qr1d34444b4a6244dc2a42872c060366423.JPG', 'qr18dc604bbbaed49d49f6630dd8c99b738.JPG', 'qr1e54d5be58aa94727a04c2631837c4bce.JPG', 'qr1d67308d87cfe44e48716ee806e321f02.JPG', 'qr16ac566fc2daf4c638043ed1270242471.JPG', 'qr189347c4a5ecc45878682d2f1f51e1cb6.JPG', 'qr163769c95bc964bf0a131f394a3411cae.JPG', 'qr1335fdf6c0d4542d0b60bc1d087b0c8cd.JPG', 'qr149d69340001f4d5c9add98fed9aaa9ca.PNG', 'qr1e5c97f4409f9457f8b23e3f094211b3a.PNG', 'qr1b51c1af1adce49158e39c7a8445f18e7.PNG', 'qr1c30595aa64184a6f910715ca52c59dd0.PNG', 'qr18b3d47db582d4b8eb2aa00b96c933ba6.PNG', 'qr105f292480ec34250925ccdf6f6045811.PNG', 'qr1bd1f900c73a1410c99e26f2fc9544725.PNG', 'qr1fa4d39a6750949e08309b57f1bd722e8.PNG', 'qr1a1bbf5a45cdd4350bd769621e30a5a4b.PNG', 'qr14aad5026f1624426911f40081ea704fa.PNG', 'qr1f5ea81899d694f7083519b9a80d672bc.PNG', 'qr18cb1ed8f814347b98c47ebda17a81775.PNG', 'qr1616904d3f87949b1b79deb610fc0fde4.PNG', 'qr1ff16fc2d72d54562bd660ca5a00ea6a3.PNG', 'qr1cff1f3afb62b47d38b36991da9d6a5e6.PNG', 'qr1ee7d0beef78d47b292f8fb80e29723d8.PNG', 'qr1a762a58e29e94414906eab193264d1ce.PNG', 'qr1bf6dd8208e6d401d9910524a9a9d8ba0.PNG', 'qr1a13884d614a2483a9cb13d5d5c6c7f08.PNG', 'qr1716753b57de94d2b838007568d8a2147.PNG', 'qr11d55053368834b189e8775c97ec2723e.PNG', 'qr15b62b28b6090490cb43c98d167454707.PNG', 'qr156a748cf4e984b9b8ed17388131e98f4.PNG', 'qr1e69425808c26487b9e3d704b0afe8bed.PNG', 'qr12bcfa1df49c24a9fbb176c40436ab794.PNG', 'qr192eeda64e58f4d69b3872b38d4d4fa61.PNG', 'qr139507c23e96a4723900af73a0a8c9d49.PNG', 'qr1b6d1d2fd24c44befb21432d3f0a5fce1.PNG', 'qr1215bc2de3be8470587110ac8cfa19538.PNG', 'qr1781c66451108498ea2d947cd8b8fd7a4.PNG', 'qr15b892c662b45480cbd3662015db5a562.PNG']
# all the files in `cleanupdir`
cleanupfiles = os.listdir(cleanupdir)

# write backups of `logdl` and `logct`
fp_bakdl = os.path.join( os.path.split(fp_logdl)[0]           # backup file for logdl
                       , f"{os.path.split(fp_logdl)[1]}.bak" )
with open(fp_bakdl, mode="w") as fh:
    json.dump(logdl, fh, indent=2)
fp_bakct = os.path.join( os.path.split(fp_logct)[0]           # backup filev for logct
                       , f"{os.path.split(fp_logct)[1]}.bak" )
with open(fp_bakct, mode="w") as fh:
    json.dump(logct, fh, indent=2)

# map filenames in `files` to the filenames in `cleanupfiles`
to_base            = lambda fn: re.search(r"^qr1[a-z0-9]{32}", fn)[0]  # type:ignore
files_from_cleanup = lambda x: [ f for f in cleanupfiles if re.search(fr"^{x}\.[a-zA-Z]+$", f) ]

filesbase = { f:to_base(f) for f in files }                           # full filename mapped to basename
mapper    = { k:files_from_cleanup(v) for k,v in filesbase.items() }  # { <filename in `files`>: [<list of matched filenames in `cleanupfiles`>] }
assert all(len(v) == 0 or len(v) == 1 for v in mapper.values())
mapper    = { k:v[0] for k,v in mapper.items() if len(v) }            # { <filename in `files`>: <single matched filename in `cleanupfiles`> }. we have removed entries with no filenames
assert len(mapper.keys()) == len(files)

# map filenames in `logdl` with their replacement (filenames in `cleanupfiles`)
# then,
#   > move the proper files from `cleanupdir` to `outdir`
#   > update `logdl` with the replacement filenames.
#   > write `logdl` to output
is_in_logdl  = lambda fn,_logdl: any( fn in v for v in _logdl.values() )
mapper_logdl = { k:v for k,v in mapper.items() if is_in_logdl(k, logdl) }  # { <to replace>: <replacement> }
for k,v in mapper_logdl.items():
    shutil.copy2(os.path.join(cleanupdir, v), os.path.join(outdir, v))
    for idx, fn_arr in logdl.items():
        logdl[idx] = [ v if item == k else item for item in fn_arr ]
# notinout = [ fn for v in logdl.values() for fn in v if fn not in os.listdir(outdir) ]
# print([ fn for fn in notinout if fn not in cleanupfiles ])
# assert all(fn in os.listdir(outdir) for v in logdl.values() for fn in v)
with open(fp_logdl, mode="w") as fh:
    json.dump(logdl, fh, indent=2)

# finally, move the thumbnailed and compressed versions of each file in
# `mapper_logdl` to `outdir`. there's no need to update `logct`
find_ct   = lambda fn: [ f for f in cleanupfiles
                         if re.search(fr"^{to_base(fn)}(_compress|_thumbnail)\.[^\.]+$", f) ]
mapper_ct = { f:find_ct(f) for f in files }                  # { <problematic filename>: <array of compress/thumbnail equivalents in `cleanupdir`> }
assert all(any(_ in x for x in logct.values())               # all values of mapper_ct are also values of `logct`
           for v in mapper_ct.values()
           for _ in v )
for k,v in mapper_ct.items():
    for fn in v:
        shutil.copy2(os.path.join(cleanupdir, fn), os.path.join(outdir, fn))
assert all(fn in os.listdir(outdir) for v in mapper_ct.values() for fn in v)  # all values of `mapper_ct` are in `outdir`

