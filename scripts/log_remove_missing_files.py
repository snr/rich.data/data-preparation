# ***************************************************************************
# remove images that are in `src/logs/log_iconography_outfile.json`
# but not in `out/iconography/images`.
#
# log_iconography_outfile.json is a JSON mapping an ID of the input
# iconography CSV to a file name that should be in `out/iconography/images`.
#
# after removing missing images, when rerunning `main.csv`, the images that
# have been removed from the log will be redownloaded and reprocessed.
# ***************************************************************************

import typing as t
import argparse
import json
import os

# the files to remove. this should be replaced
# by what is printed to the console in `Iconography.pipeline()`
# toremove = ['qr19726c6a3d9754180a99d99a5c3dcc978.jpg', 'qr1b00bf4ee1d3144f49b285aae7877c2ee.jpg', 'qr12d6cee706d964304bfedc7581187173c.JPG', 'qr16b321ae75ba24ba4a27d774a807afa2d.JPG', 'qr1eec3e2e01d054a42b46972e8d8ef74b1.JPG', 'qr151d9dbdae1b549ddac3aa1f9ad0764fd.JPG', 'qr15ca4192ee0854f80be1c96adecafab0c.JPG', 'qr160de4f74c0984444bbd90dbf1e9f95c3.JPG', 'qr1ca2057c7df3b4c789094a04f8b2a69ee.JPG', 'qr1687e7d606594480287bb7974d0fef6aa.PNG', 'qr1e81f311922004425a46478244d61ab54.jpg', 'qr1a891e012d2954a619bf7f17df985553a.jpg', 'qr11f7ff5d706f94e6dbc932436e2d973f1.jpg', 'qr1ce31edc7561048ee82779a6c31626385.jpg', 'qr14d30bdb071d44d83891a3bd84cb181d3.jpg', 'qr18fa3214da88c4a15ac4c95bc06da117e.jpg', 'qr1e3c0f81896894012a031942e7ea6a219.jpg', 'qr16ebbffe038744dadad6ab331440e6d58.jpg', 'qr1d92f76c9648a45efaa3cfd603a6bf6f2.jpg', 'qr112c2a11747764ea38e30aaf4a29de3ac.jpg', 'qr19127caf2595e49f796f0c5e7f65e3ec8.jpg', 'qr12b8cfc5be2d949198200ca4f27bde593.jpg', 'qr18540db1a5fbc47f9877ffc69e77501bd.jpg', 'qr147dd6886e8aa4bf587795015c63148c8.jpg', 'qr1407e96b0797e4325abdadb2ee04a360c.jpg', 'qr1abf60fdfd2d447aca3f2fbf5ddf4bf99.jpg', 'qr1a9049f491c99425b95f2f2e73c26b16e.jpg', 'qr17c85bfec9021453fa61d568ea2ed9df1.jpg', 'qr17cd402052fe843f2a2b158d300245acd.jpg', 'qr1fd7d6355d6c14fccace4fc371933aa87.jpg', 'qr164b046729c7d490c978019fd9cf21dae.jpg', 'qr152702ece34d64800806185f544c847ee.jpg', 'qr156f2a74c57b24a3282620e8c5c548646.jpg', 'qr1883526ed885446028b05f8465a31dddc.jpg', 'qr173b581a14102422fac7e4701155c49ed.jpg', 'qr131e7cdbfe4b34e108db8779178c58dd5.jpg', 'qr151ba2139504e4a7396bb5cfef85ac943.jpg', 'qr14512e1c51ad245f3a63e6b25ce27c4f0.jpg', 'qr158f6decc413648f88f99cbf6c26f57d2.jpg', 'qr1a2b8b50800c5487e8c8355f4c924e56a.jpg', 'qr1eb8a2d939d774b32afbb5489cdd07047.jpg', 'qr1eaaf617a3d4d473dbd05bb1429c2a675.jpg', 'qr17df758cc5b214dfa93793628021cc062.jpg', 'qr1713aba4b03434b4ab4f80288ca27c4e1.jpg', 'qr1fd104faaef9b4611bbc9e59fdf22012b.jpg', 'qr147034c9215a04a17a99f7c230c368835.jpg', 'qr112f5c68201bb455eb5329722719fd5b1.jpg', 'qr1b6925fcc36524fb696a7e0f48cc76dba.jpg', 'qr16dfbe1fd1ac44739b08a5ffdcd10b509.jpg', 'qr13c961da9412247529b64249083d00e28.jpg', 'qr141626034650c4d90ace3a90fc07fbf02.jpg', 'qr11ec2051d6485410e85cc3f8c87cc2b05.jpg', 'qr11746c71a8b5e4b5a86eb58d9153afacd.jpg', 'qr1f5c644aa2e7348239069fe32d836cfa6.jpg', 'qr11134c2ff6e894feb9e0ac15fec187d3b.jpg', 'qr155ccd85c90b54265ababb33417f450af.jpg', 'qr1c4787d27da1746f2a89c0d39c72b65ed.jpg', 'qr1e83bb351d24d42a1a253d3b34dde450b.jpg', 'qr1308ae7f7f6d04a24a3446d0befb09514.jpg', 'qr171ce863e0773447e9353f2e3346f1771.jpg', 'qr1cc58f25146254837a1f632ef9e2f35e9.jpg', 'qr136bc258c5bd34d0695cf83ded5ddad4b.jpg', 'qr18636b82352b641da801918e92564b921.jpg', 'qr12eb19dec7b6f4baab04290ea836b0a2f.jpg', 'qr13ea4c8a2efc44a0b858f4781658875e1.jpg', 'qr1917b3e7eea754656bb690dd20c00c6c2.jpg', 'qr17bc0ca6a238e4a7a82c5492e8f7a8458.jpg', 'qr1a7877e263828445f9f4d7f4d63cb586d.jpg']

def process_args(filename:str, deldir:str):
    """process the input argument"""
    infile = os.path.abspath(filename)                         # input
    outfile = infile                                           # output (updated json is written in place, with a backup created)
    bakfile = os.path.join( os.path.split(infile)[0]           # backup file
                          , f"{os.path.split(infile)[1]}.bak" )
    if not os.path.isfile(infile):
        raise FileNotFoundError(f"{infile} does not exist !")
    deldir = os.path.abspath(deldir)
    if not os.path.isdir(deldir):
        raise FileNotFoundError(f"{deldir} directory does not exist !")
    return [ infile, outfile, bakfile, deldir ]


def find_missing_files(infile, deldir) -> t.List[str]:
    """
    find all the files mentionned in `infile` that are missing from `deldir`
    """
    with open(infile, mode="r") as fh:
        data = json.load(fh)
    # return a deduplicated list of filenames if those filenames don't point to a file in `os.listdir`.
    return [ f for v in data.values() for f in v if f not in os.listdir(deldir) ]


def update_logger(infile, outfile, bakfile, toremove:t.List[str]):
    """
    run the main process, that is, to remove
    everything that is in `toremove` from `data`"""
    # read source data
    with open(infile, mode="r") as fh:
        data = json.load(fh)

    # back it up to `backfile`
    with open(bakfile, mode="w") as fh:
        json.dump(data, fh, indent=2)

    # pop the missing files from the values of `data`
    remove_missing_files = lambda filelist: [ f for f in filelist if f not in toremove ]
    data = { k:remove_missing_files(v) for k,v in data.items() }

    # write to file
    with open(outfile, mode="w") as fh:
        json.dump(data, fh, indent=2)

    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser( prog="log_remove_missing_files"
                                    , description="remove missing filenames from log")
    parser.add_argument("filename", help="the log file to remove missing filenames from")
    parser.add_argument("deldir", help="the directory where images will be searched (non-recursively)")
    parser.add_argument( "-s", "--show"
                       , help="print the filenames that will be deleted before continuing"
                       , action="store_true")

    args = parser.parse_args()

    infile, outfile, bakfile, deldir = process_args(args.filename, args.deldir)
    toremove = find_missing_files(infile, deldir)
    if args.show:
        print("\n")
        print(toremove)
    cont = input(f"\n>>> {len(toremove)} filenames will be removed from `{args.filename}`. do you with to continue ? [y/n]")
    if cont == "y":
        update_logger(infile, outfile, bakfile, toremove)
    else:
        print("\n>>> aborted. exiting...")
        exit(0)





