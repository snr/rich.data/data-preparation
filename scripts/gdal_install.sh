#!/usr/bin/env bash

[[ "$(lsb_release -d)" =~ Description:[^A-Za-z]+([A-Za-z]+) ]] && os="${BASH_REMATCH[1]}"
[[ $os != "Ubuntu" ]] && echo -e "\n* this script only works with Ubuntu *\n* exiting *" && exit 1

# install the gdal package and dev librairies
sudo add-apt-repository ppa:ubuntugis/ppa && sudo apt-get update
sudo apt-get update
sudo apt-get install gdal-bin
sudo apt-get install libgdal-dev
export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal

# check if a python venv is active
[[ -z "$VIRTUAL_ENV" ]] \
&& echo -e "\n\n* EXCEPTION IN 'install_gdal.sh' - please source a python virtualenv and run again. *\n* exiting... *" \
&& exit 1

# get the installed gdal version
gdalversion=$(ogrinfo --version)
[[ ${gdalversion} =~ ([0-9]|\.)+ ]] && gdalversion="${BASH_REMATCH[0]}"
echo $gdalversion

pip install GDAL==$gdalversion
