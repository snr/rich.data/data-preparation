# SCRIPTS

Scripts utilitaires et micro-CLI pour des opérations autour de nos données.

- `cleanup2out.py`: déplacer dans `out/` des fichiers image qui sont mentionnés 
  dans les logs mais qui ont été par erreur mis dans le dossier-poublelle `cleanup.`
- `correct_icono_institution.py`: corriger automatiquement et en masse des noms
  d'institution invalides dans notre tableur d'iconographie.
- `deduplicate_piped_col.py`: dédupliquer les valeurs présentes dans une colonne
  multivaluée (produit un tableur CSV d'une colonne, avec une valeur par colonne,
  sans doublons). Utilisé pour vérifier les erreurs dans les colonnes `theme` et `entités nommées`
- `flatten.py`: applatir un fichier contenant des sous-fichiers
- `gdal_install.sh`: installer GDAL sur macOS/linux.
- `log_remove_missing_files.py`: si un fichier de log fait mention de noms de fichiers
  qui ne sont pas dans `out`, les supprimer
- `prepare_icono_dir.py`: préparer le dossier `in/iconography` pour qu'il ait la bonne structure
- `select_images.py`: sélectionner des images au hasard.
