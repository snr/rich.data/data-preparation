from rapidfuzz.distance import Levenshtein as rf_lev
from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from PIL import Image, UnidentifiedImageError
from unidecode import unidecode
from datetime import datetime
from statistics import mode
import rapidfuzz as rf
from tqdm import tqdm
import pandas as pd
import numpy as np
import typing as t
import validators
import requests
import time
import json
import os
import re

from src.utils.constants import ( IN_ICONOGRAPHY, LOGS, OUT_ICONOGRAPHY
                                , ICONO_HEADERS, MUSEUM_CODES, CLEANUP
                                , IN_TO_COMPLETE, OUT_TO_COMPLETE)
from .utils.io import (read_df, copy_resource, write_df)
from .utils.strings import (idx_simple_match, idx_complex_match
                           , idx2filename, iiif_url_match, build_uuid
                           , date2range, splitcell)
from .utils.conditions import hasapi, isempty
from .utils.api import iiif_extract
from .shared import Shared


# ********************************************************************************
# clean and process the iconography dataset to
# prepare it for web publishing
#
# about pandas:
# - https://pandas.pydata.org/docs/user_guide/10min.html#min
# - https://pandas.pydata.org/docs/user_guide/basics.html
# - https://pandas.pydata.org/docs/user_guide/basics.html#basics-dtypes
# ********************************************************************************


class Iconography(Shared):
    """
    class for the `Iconography` cleaning pipeline
    """
    def __init__(self):
        self.incsv      = os.path.join(IN_ICONOGRAPHY, "iconography_viviennepalaisroyalbanque_20240719.csv")

        self.logdl      = os.path.join(LOGS, "log_iconography_outfile.json")
        self.logct      = os.path.join(LOGS, "log_iconography_compressthumbnail.json")

        self.outcsv     = os.path.join(OUT_ICONOGRAPHY, "iconography_clean.csv")
        self.cleanupdir = os.path.join(CLEANUP, "iconography")
        self.inimg      = os.path.join(IN_ICONOGRAPHY, "images")
        self.outimg     = os.path.join(OUT_ICONOGRAPHY, "images")

        self.inmiscsv   = os.path.join(IN_TO_COMPLETE, "missing.csv")  # manually added filenames
        self.outmiscsv  = os.path.join(OUT_TO_COMPLETE, "missing.csv")  # rows where the `filenames` column is empty

        # we don't use the `io.read_df`, it causes weird stuff,
        # best to use a specific function here.
        self.df         = pd.read_csv( self.incsv, sep="\t", quotechar='"'
                                     , header=0, skip_blank_lines=True
                                     , names=ICONO_HEADERS, index_col=False
                                     ).set_index("index")
        self.df_actors  = pd.DataFrame(columns=["src", "first_name", "last_name", "date"])   # index of actors to manually deduplicate
        self.lastbnf    = None  # time logger for BnF image download

        if os.path.isfile(self.inmiscsv):  # list of images with no matched image file
            self.df_missing = read_df(self.inmiscsv, None, 0)
        else:
            self.df_missing = pd.DataFrame(columns=["filenames", "source_url", "iiif_url"])

        # simplify the string `x`: remove spaces, normalize accents, to lowercase
        # type x  : string
        # returns : string
        self.simplify = lambda x: unidecode( "".join(filter(str.isalnum, x)).lower() )

        return


    def pipeline(self):
        """
        main function of the cleaning pipeline.

        * drop empty or duplicate rows and rows with no URL info
        * split columns with pipe-separated cells into lists
        * disambiguate data
        * simplify and normalize some columns (dates...)
        * match local filenames
        * extract files (by downloading them or getting them locally)
        * compress + create a thumbnail the images
        * replace handmade ids by UUIDs
        * write the outputs

        """
        tqdm.pandas()

        # simple operations
        # drop useless rows
        self.df = (self.df.replace(r"^(\s|\n)*$", np.nan, regex=True)
                          .dropna(how="all")
                          .drop(columns="internal_note")
                          .drop_duplicates())
        self.df = self.df.loc[ self.df.index.notna()
                             & ( self.df.source_url.notna() | self.df.iiif_url.notna() ) ]

        # add 2 useful columns:
        self.df["idxarr"] = self.df.index.map(self.decompose_index)  # decompose indexes into arrays
        self.df["id_richelieu"] = self.df.index.str.strip()                 # store the original index into a separate column

        # normalize the simplest columns + disambiguate the values
        self = (self._pipe( splitcell
                          , cols=[ "author_1", "author_2", "publisher"
                                 , "technique", "named_entity", "theme"
                                 , "id_place", "iiif_folio", "inscription" ]
                          , retype={"iiif_folio": "int"})
                    ._pipe(self.produced_represents)
                    ._pipe(self.canonicalize)
                    ._pipe(self.norm_url)
                    ._pipe(self.norm_institution)
                    ._progress_apply(self.norm_date, axis=1)
                    ._progress_apply(self.norm_rights, axis=1))

        # link rows to image files (by redownloading the images
        # or, less ideally, by matching local files)
        mask_local = self.df.iiif_url.isna()
        mask_iiif  = self.df.iiif_url.notna()
        self.df["filenames_local"] = self.df.idxarr.progress_apply(self.idxarr2localfilename)
        self.df.loc[mask_local] = self.df.loc[mask_local].progress_apply(self.outfile_local, axis=1)  # type: ignore
        self.df.loc[mask_iiif] = self.df.loc[mask_iiif].progress_apply(self.outfile_iiif, axis=1)     # type: ignore

        # update the dataframe with file names
        self.df["filenames"] = [ [] for _ in range(self.df.shape[0]) ]
        self._pipe(self.insert_filenames_outfile)
        self.images_not_in_out()

        # debug prints. can be deleted
        # self.df.to_csv("_.csv", sep="\t")
        # print("> dataset size :", self.df.shape[0], "rows")
        # print(">", self.df.filenames_local.apply(len).gt(0).sum(), "rows with local files matched")
        # print(">", self.df.filenames.apply(len).gt(0).sum(), "rows with filenames matched (local or not)")
        # print(">", len([ f for f in self.df.filenames.explode()
        #                   if f not in os.listdir(self.outimg)
        #                   and not pd.isna(f) ])
        #      , "filenames matched but not present in `self.outimg`")

        # replace the df's index by uuids (stored in `self.logdl`)
        self.df["uuid"] = [ build_uuid() for _ in range(self.df.shape[0]) ]# self.df.uuid.apply(lambda x: build_uuid() if pd.isna(x) else x)  # i don't know why, but we lose some UUIDs => create new ones
        self.df.index = pd.Index(self.df.uuid)
        self.df = self.df.drop(columns=["uuid"])
        self.df.index.name = "index"

        # process images
        (self._progress_apply(self.fiximages, axis=1)
             ._progress_apply(self.compressthumbnail, axis=1)
             ._pipe(self.insert_filenames_compressthumbnail))

        # finish cleaning the files + write the csvs
        (self._pipe(self.output_missing)
             ._pipe(self.cleanup))

        self.df = self.df.drop(columns=["idxarr", "filenames_local"])
        print(self.df.columns)
        write_df(self.df, self.outcsv)
        write_df(self.df_missing, self.outmiscsv)

        return self


    def images_not_in_out(self) -> t.List[str]:
        """
        find all rows mentionned in `self.df.filenames` that are not present in
        `self.outimg`: if too many files are missing from the output, , there is
        probably an error, either in the logger (self.logdl), or in the image
        files that are deleted by `self.cleanup()`.
        """
        files = self.df.filenames.explode().loc[ self.df.filenames.explode().notna() ].unique()
        notfound = [ f for f in files if f not in os.listdir(self.outimg) ]
        if len(notfound) > 5:
            print("\n\n", notfound, "\n")
            print(set(re.search(r"\.[^\.]+$", s)[0] for s in notfound))  # type:ignore
            print(f"* Iconography.images_not_in_out(): the above {len(notfound)} images "
                  +"are present in `self.df` but not in `self.outimg`. there may be a "
                  +"problem with your `self.logdl`. to remove the missing images from "
                  +"`self.logdl`, run `scrips/update_log_3.py` before rerunning "
                  +"`Iconography.pipeline()`. the missing images will then will be "
                  +"redownloaded and added to the log.\n")

        return notfound


    def norm_url(self, df:DFType) -> DFType:
        """
        process and normalize urls:
        * validate them
        * remove the fragment
        * where possible, correct the IIIF url, or extract it from df.source_url;
        """
        def source2manifest(s:str) -> str|float:
            """
            for the bnf entries, generate a IIIF url from the `source_url` column
            :param  : the url in df.source_url
            :returns: a valid IIIF BnF url
            """
            try:
                ark = re.search(r"(?<=ark\:/)(.+)$", s)[1]   # type: ignore
                return f"https://gallica.bnf.fr/iiif/ark:/{ark}/manifest.json"
            except TypeError:  # no ark found
                return np.nan

        df.source_url = (df.source_url
                           .apply(lambda x: x if validators.url(x) else np.nan)    # validate the url
                           .replace("#.+?$", "", regex=True) )                     # remove the url fragment
        df.iiif_url = (df.iiif_url
                             .apply(lambda x: x if validators.url(x) else np.nan)  # validate the url
                             .replace("#.+?$", "", regex=True) )                   # remove the url fragment

        # extract a IIIF manifest from a BnF URL
        mask = df.institution.eq("BnF") & df.iiif_url.isna() & df.source_url.notna()  # bnf rows with an source_url but no manifest
        df.loc[mask, "iiif_url"] = df.loc[mask, "source_url"].apply(source2manifest)

        # extract a IIIF manifest from a Paris Musées URL.
        # not done, we will generate our own IIIF images when no IIIF manifest is present.
        # see PM API: https://www.parismuseescollections.paris.fr/fr/recuperer-des-donnees
        # and especially the field `fieldUrlAlias`, which contains the URL we have in `df.source_url`
        # mask = df.institution.eq("Musée Carnavalet") & df.iiif_url.isna() & df.source_url.notna()

        return df


    def decompose_index(self, idx:str) -> t.List[str]:
        """
        decompose an index as they are written in the input
        (ex: BNF1, BNF 1, BNF_1, BNF 1 à 3) into an array of
        normalized indexes ([BNF_1, BNF_2, BNF_3]).

        :param idx : the input index
        :returns   : an array of normalized indexes
        """
        idx = idx.strip()                      # type: ignore ;
        idx_simple = idx_simple_match(idx)     # normalized notation if simple, `""` if not simple
        idx_complex = idx_complex_match(idx)   # array of normalized index if complex, `[]` if not complex

        # no match could be done
        if idx_simple == "" and not len(idx_complex):
            idx = re.sub(r"\s+", " ", idx.strip()).replace(" ", "_")
            idxarr = [ idx ]
        # simple match
        elif not len(idx_complex):
            idxarr =  ([ idx_simple ]
                       if not pd.isna(idx_simple) and len(idx_simple) > 0
                       else [])
        # complex match
        else:
            idxarr = [ idx for idx in idx_complex ]
        return idxarr


    def idxarr2localfilename(self, idxarr:t.List[str]) -> t.List[str]:
        """
        match a local image file based on `idxarr`, an array of
        normalized indexes for a row (given that an index is a
        pointer to one or several files)
        :param idxarr : the index array we're working on
        :returns      : the list of matched filenames
        """
        filenames_local = []
        images_in = [ os.path.join(self.inimg, f)                    # full path for all images
                      for f in os.listdir(self.inimg)
                      if os.path.isfile(os.path.join(self.inimg, f)) ]

        for idx in idxarr:
            for i in images_in:
                fname = os.path.basename(i)  # filename without path
                id_img = re.sub(r"(?<=.)\..*?$", "", os.path.basename(i)).lower()  # filename without path and extension. `(?<=.)` avoids matching hidden files (begin with `.`)

                # WARNING: changing these regex can cause fake matches !!!!!!!
                # uncomment the print block to check the result
                if (idx == id_img.lower()
                    or re.search(fr"^{idx}[^\d].+$", id_img.lower()) ):  # matches `BNF_346`, `BNF_346-2` but not `BNF_3462`
                    filenames_local.append(fname)
                elif ( not any(e in id_img for e in ["_", "-", " "]) ):  # if there are not any risky characters in the filename, try matching with a very simplified `idx`
                    if re.sub(r"[\s_-]", "", idx) == id_img:
                        filenames_local.append(fname)
        # print(f"*{arr}*", filenames_local)
        return filenames_local


    def outfile_local(self, ro:SType) -> SType:
        """
        copy local files  to `self.outimg` and update the logger
        """
        if not self.isinlog_outfile(ro.id_richelieu):                                   # type: ignore
            filenames = copy_resource( ro.filenames_local
                                     , self.inimg
                                     , self.outimg )
            self.writelog_outfile(ro.name, filenames)                           # type: ignore
        return ro


    def outfile_iiif(self, ro:SType) -> SType:
        """
        get images to `self.outimg` using the IIIF protocol.
        if there is a problem with the IIIF download, move to
        local files to `self.outimg`.
        finally, update the logger
        """
        def download_resource(self, url:str, extension:str) -> str:
            """
            download a file from requests and write it to `self.outimg`

            :param url: the url to download the file from
            :param str: the format to write the file to
            :returns: the image name.
            """
            fname = f"{build_uuid()}.{extension}"
            outfile = os.path.join(self.outimg, fname)

            # avoid `429 too many requests` on BnF images:
            # wait 20 seconds between requests
            if "bnf" in url and self.lastbnf is not None:
                _ = (datetime.now() - self.lastbnf).total_seconds()  # time since the last bnf request in seconds
                time.sleep(20 - _ if _ < 20 else 0)                  # make sure 20 seconds passed
                self.lastbnf = datetime.now()                        # update the logger
            elif "bnf" in url:
                self.lastbnf = datetime.now()

            # run request + dl image
            with requests.get(url, stream=True) as r:
                if r.status_code != 200:
                    print(url)
                    print(r.text)
                    r.raise_for_status()
                else:
                    with open(outfile, mode="wb") as fh:
                        for chunk in r:
                            fh.write(chunk)
            return fname

        if not self.isinlog_outfile(ro.id_richelieu):  # type: ignore
            filenames = []

            # iiif request: extract relevant resources + download the images
            resources = iiif_extract(ro.iiif_url, ro.iiif_folio)
            if len(resources):
                for r in resources:
                    filenames.append( download_resource(self, r[0], r[1]) )  # type: ignore

            # no IIIF resource matched => work from the local files
            else:
                filenames = copy_resource( ro.filenames_local
                                         , indir=self.inimg
                                         , outdir=self.outimg )

            # finally, append to log
            self.writelog_outfile(ro.id_richelieu, filenames)  # type: ignore
        return ro


    @staticmethod
    def norm_institution(df:DFType) -> DFType:
        """
        normalize the institution notation, using either
        data in the `institution` column or the first letters
        of df.idx.
        """
        simpcol = lambda x: x.str.lower().str.strip()            # simplify a df column. x = a pd.Series of type str

        mc_vals        = [ v.lower() for v in MUSEUM_CODES.values() ]
        mc_keys        = [ k.lower() for k in MUSEUM_CODES.keys() ]
        mc_keys_mapper = { k.lower().strip():v for k,v in MUSEUM_CODES.items() }  # { <simplified key> : <full institution name> }
        mc_vals_mapper = { v.lower().strip():v for v in MUSEUM_CODES.values() }   # { <simplified institution name>: <full institution name> }

        df["idxbase"] = df.index.to_series().str.extract(r"(^[A-Za-z]+)")
        df["institution2mc_vals"] = df.institution.pipe(simpcol).isin(mc_vals)
        df["institution2mc_keys"] = df.institution.pipe(simpcol).isin(mc_keys)
        df["idxbase2mc_keys"]     = df.idxbase.str.lower().isin(mc_keys)

        df["institution_clean"] = ""
        df.loc[ df.institution2mc_vals, "institution_clean" ] = df.loc[ df.institution2mc_vals, "institution" ]
        df.loc[ df.institution2mc_keys, "institution_clean" ] = (df.loc[ df.institution2mc_keys, "institution" ]
                                                                   .pipe(simpcol)
                                                                   .map(mc_keys_mapper))
        df.loc[ df.idxbase2mc_keys, "institution_clean" ]     = (df.loc[ df.idxbase2mc_keys, "idxbase" ]
                                                                   .pipe(simpcol)
                                                                   .map(mc_keys_mapper))
        print(df.loc[ df.idxbase2mc_keys | df.institution2mc_vals | df.institution2mc_keys ].shape[0] == df.shape)
        df.institution = df.institution_clean

        assert df.institution.isna().sum()==0 and df.institution.eq("").sum() == 0, \
               "`norm_institution()`: institutions have been deleted by this function !"

        df = df.drop(columns=[ "institution2mc_vals", "institution2mc_keys"
                             , "idxbase2mc_keys", "institution_clean", "idxbase"])
        return df


    @staticmethod
    def norm_date(ro: SType) -> SType:
        """
        normalize the date notation: from "human readable" date notation,
        build a list of [min_year, max_year]. from this will be created an
        INT4RANGE postgresql column

        :param ro: the csv row being processed
        """
        if not isempty(ro.date_corr):
            ro["date"] = date2range(ro.date_corr)
        elif not isempty(ro.date_source):
            ro["date"] = date2range(ro.date_source)
        return ro


    @staticmethod
    def norm_rights(ro:SType) -> SType:
        # TODO => ACTUALLY DONE IN STEP 2_data2sql
        return ro


    def produced_represents(self, df:DFType) -> DFType:
        """
        convert the `produced_richelieu` and `represents_richelieu`
        columns to bool
        :raises AssertionError: if values other than `oui, non, np.nan`
                                are found in the columns (case insentive)
        """
        df.produced_richelieu = (df.produced_richelieu.astype(str).str.lower()
                                                      .astype(str).str.strip()
                                                      .map({ "oui": True, "non": False }))
        notin = df.loc[ ~df.produced_richelieu.isin([ np.nan, True, False ]), 'produced_richelieu' ].unique()  # all values from `produced_richelieu` that are invalid, as a list

        assert len(notin) == 0 \
               , f"on df.produced_richelieu column: values `{ notin }` are not in `[ np.nan, True, False ]`"

        df.represents_richelieu = (df.represents_richelieu.astype(str).str.lower()
                                                          .astype(str).str.strip()
                                                          .map({ "oui": True, "non": False }))
        notin = df.loc[ ~df.represents_richelieu.isin([ np.nan, True, False ]), 'represents_richelieu' ].unique()  # all values from `represents_richelieu` that are invalid
        assert len(notin) == 0 \
               , f"on df.represents_richelieu column: values `{ notin }` are not in `[ np.nan, True, False ]`"
        return df


    def col2canon(self, col:SType) -> SType:
        """
        disambiguate the values in `col`: simplify all its values,
        group them together and choose a single value for all those
        that have been grouped together.

        :param col : the column to process. must be a pipe-separated column.
        :returns   : the same column, with simplified values
        """
        vals     = []  # all unique items in our lists
        simpmap  = {}  # dict of { <simplified term>: [<array of full terms>] }, later retyped to { <simplified term>: <canonical full term> }
        simpvals = []  # array of simplified terms in vals

        vals     = [ v for v in col.explode().unique() if not pd.isna(v) ]
        simpvals = [ self.simplify(v) for v in vals if self.simplify(v) not in simpvals ]
        simpmap  = { s:[] for s in simpvals }
        for v in vals:
            simpmap[ self.simplify(v) ].append(v)
        simpmap = { k:mode(v) for k,v in simpmap.items() }  # extract the mode

        col = ( col.apply(lambda x: [ self.simplify(_) for _ in x ])
                   .apply(lambda x: [ simpmap[_] for _ in x ]) )

        return col


    def canonicalize(self, df:DFType) -> DFType:
        """
        clusterize the values in the columns in `cols` to extract and use
        a single canonical orthograph for all variants of a term.
        """
        cols = ["author_1", "author_2", "technique", "publisher"
                , "theme", "named_entity"]

        for col in cols:
            df[col] = self.col2canon(df[col])
        return df


    def output_missing(self, df:DFType) -> DFType:
        """
        update `self.df_missing`, with the df rows containing no images.
        this DF will be written to `out/missing.csv`.
        """
        # add the newly missing rows to `df_missing`
        df_missing_add = (df.loc[ df.filenames.apply(len).eq(0)                  # rows to add to `self.df_missing`
                                , ["id_richelieu", "filenames", "iiif_url", "source_url"] ]
                            .set_index("id_richelieu"))
        self.df_missing = (pd.concat([self.df_missing, df_missing_add], axis=0)  # concatenate the new df with the one used in input
                           [["filenames", "iiif_url", "source_url"]])            # reorder the columns (necessary to reuse `missing.csv` as an input)

        # retroconvert the list of filenames to pipe-converted values
        self.df_missing.filenames = (self.df_missing
                                         .filenames
                                         .apply(lambda x: "|".join(_ for _ in x))
                                         .replace(r"(^\|\|$)", "", regex=True))
        return df


    def fiximages(self, ro:SType) -> SType:
        """
        fix errors with the downloaded images:
        * in some cases, the extension doesn't match the actual
          format. rename the file so that it has the proper format
        * in other cases, the image is corrupted => remove it.
        update `ro.filenames`, by changing the filenames
        or deleting the names of corrupted files.
        """
        Image.MAX_IMAGE_PIXELS = 10**10  # avoid DecompressionBombError
        fnames = []                      # list of image names. will replace `ro.filenames`
        for fn in ro.filenames:
            try:
                with Image.open(os.path.join(self.outimg, fn)) as img:
                    ext = re.search(r"\.([^\.]+)$", fn)[1]     # type: ignore . file extension
                    extfmt = "jpeg" if ext == "jpg" else ext   # img.format is `JPEG` for jpeg files, with the extension `.ext`

                    # rename the file + fix format errors if necessary
                    # if the file is renamed, `self.logdl` needs to be
                    # updated with the new filename, otherwise it will
                    # lead to very hard to debug `FileNotFoundError`s
                    # when rerunning this pipeline more than once.
                    if str(img.format).lower() != extfmt:
                        _fn = fn
                        fn = re.sub(f"{ext}$", str(img.format).lower(), _fn)  # update the extension to match the file format
                        os.rename( os.path.join(self.outimg, _fn)
                                 , os.path.join(self.outimg, fn))
                        self.updatelog_outfile(ro.id_richelieu, _fn, fn)  # type:ignore

                    # all is good => add the image to fnames
                    fnames.append(fn)

            # `fn` has aldready been deleted because of `OSError`
            # in previous runs of this function => pass
            except FileNotFoundError:
                print(f"* Iconography.fiximages(): {fn} is missing from `self.outimg`")

            # the image is corrupted => move it to `_unprocessed and don't
            # add it to `fnames`, which means it is deleted from the dataset
            except OSError:
                if not os.path.isdir(os.path.join(self.outimg, "_unprocessed")):
                    os.makedirs(os.path.join(self.outimg, "_unprocessed"))
                    os.rename( os.path.join(self.outimg, fn)
                             , os.path.join(self.outimg, "_unprocessed", fn))

        ro.filenames = fnames
        return ro


    def compressthumbnail(self, ro:SType) -> SType:
        """
        save a compressed version + a thumbnail for each image.
        * the `thumbnail` is an image with its longest side at 300px
          and an 80% quality
        * the `compress` is an image with its longest side at 2000px
          and a 70% quality
        """
        Image.MAX_IMAGE_PIXELS = 10**10  # avoid DecompressionBombError
        fnames = ro.filenames[:]         # we need to update the filenames in a copy of `ro.filenames` to avoid changing the list during iteration (which creates an infinite loop). `[:]` copies the list rather than assigning to the variable a pointer to the original list

        for fn in ro.filenames:                         # fn is the basename of the file
            if not self.isinlog_compressthumbnail(fn):
                fn_uuid = re.search("^[a-z0-9]+", fn)[0]    # type: ignore . the uuid part of the filename
                ext = re.search("\.[^\.]+?$", fn)[0]        # type: ignore . file extension
                fn_thumbnail = f"{fn_uuid}_thumbnail{ext}"  # basename of the thumbnail file
                fn_compress = f"{fn_uuid}_compress{ext}"    # basename of the compressed file

                try:
                    with Image.open(os.path.join(self.outimg, fn)) as img:
                        # save a compressed + downsized version of the image
                        coef = 2000 / max(img.size)  # resize coefficient: the largest size of our image will be of 2000px

                        (img.resize(( int(img.size[0]*coef), int(img.size[1]*coef) ))
                            .save( os.path.join(self.outimg, fn_compress)
                                , optimize=True, quality=70))

                        # save a thumbnail of the image
                        img.thumbnail(size=(500,500))
                        img.save( os.path.join(self.outimg, fn_thumbnail)
                                , optimize=True, quality=80)
                    fnames.append(fn_thumbnail)
                    fnames.append(fn_compress)
                    self.writelog_compressthumbnail(fn, [fn_thumbnail, fn_compress])

                # errors on images are rare => remove the image names from
                # `ro.filenames` and move the images to an `_unprocessed` directory
                except UnidentifiedImageError:
                    fnames = [ f for f in fnames if f != fn ]  # remove the filename from the array of filenames, since it can't be processed
                    if not os.path.isdir(os.path.join(self.outimg, "_unprocessed")):
                        os.makedirs(os.path.join(self.outimg, "_unprocessed"))
                        os.rename( os.path.join(self.outimg, fn)
                                , os.path.join(self.outimg, "_unprocessed", fn))
                except (FileNotFoundError, OSError):
                    print(f"* Iconography.compressthumbnail(): file {fn} not found")
                    fnames = [ f for f in fnames if f != fn ]

        ro.filenames = fnames
        return ro


    def insert_filenames_outfile(self, df:DFType) -> DFType:
        """
        update the dataframe by adding the extracted filenames
        in `self.logdl` and the manually added filenames in
        `self.inmiscsv` to `df.filenames`.
        for `df_in_missing`, rename all files by uuids + move
        those files in `self.outimg`.

        to avoid reprocessing the same rows, we check
        `self.logdl` to see which rows have been processed.
        """
        # update with the log file
        with open(self.logdl) as fh:
            filenames = [ { "idx": k, "filenames": v }
                        for k,v in json.load(fh).items() ]
        df_filenames = pd.DataFrame(filenames)
        df_filenames.index = df_filenames.idx      # type: ignore
        df_filenames = df_filenames.drop(columns="idx")
        df_filenames.index.name = "index"
        df.update(df_filenames)

        # with open(self.logdl, mode="r") as fh:
        #     log = json.load(fh)

        # update with the `missing.csv` input file + move files in `df_missing` to `self.outcsv`
        mapper = {}  # { <row index>: [<array of uuid filenames>] }
        if self.df_missing.shape[0] > 0:
            self.df_missing = splitcell(self.df_missing , ["filenames"])

            for idx, fn in self.df_missing.filenames.items():
                mapper[idx] = [ copy_resource(fn, self.inimg, self.outimg) ]  # map the old filename to the new one. yes it's necessary to enclose it in a list
                # `self.df_missing` may be in conflict with `self.logdl`
                # (an index is referenced both in logdl and here, which can
                # cause duplicate UUIDs and so on...)
                # give priority to `df_missing` and update the logger with
                # the rows from `df_missing`.
                self.writelog_outfile(idx.strip(), mapper[idx][0])            # type:ignore .

            mapper = pd.DataFrame.from_dict( mapper
                                           , orient="index"
                                           , columns=["filenames"])
            df.filenames.update(mapper.filenames)

        return df


    def insert_filenames_compressthumbnail(self, df:DFType) -> DFType:
        """
        insert all compressed and thumbnailed images using
        `compressthumbnail`: `compressthumbnail` saves the
        name of processed images into a logger => retrieve
        the processed images from `self.logct` and insert
        them in the df
        """
        with open(self.logct, mode="r") as fh:
            filenames_ct = json.load(fh)

        def insert(fnarr:t.List[str]) -> t.List[str]:
            """
            actually insert the filenames
            """
            out = []            # full list of filenames: both those in the df (`fnarr`) and those in the logger (`filenames_ct`)
            for fn in fnarr:
                out.append(fn)  # add the aldready present filenames in `out`
                if fn in filenames_ct.keys():
                    for f in filenames_ct[fn]:
                        out.append(f)  # add the extra files in `filenames_ct`
            return list(set(out))

        df.filenames = df.filenames.apply(insert)
        return df


    def isinlog_outfile(self, idx:str) -> bool:
        """
        check if files for `idx` have aldready been
        downloaded or extracted and moved to `self.outimg`
        """
        if os.path.isfile(self.logdl):
            with open(self.logdl, mode="r") as fh:
                log = json.load(fh)
                return idx.strip() in list(log.keys()) #and len(log[idx.strip()]) > 0
        return False


    def writelog_outfile(self, idx:str, filenames:t.List[str]) -> None:
        """
        write a log entry:
        { <old idx>: [<array of filenames>] }
        """
        idx = idx.strip()
        if os.path.isfile(self.logdl):
            with open(self.logdl, mode="r") as fh:
                log = json.load(fh)
            log[idx] = filenames
        else:
            log = { idx: filenames }
        with open(self.logdl, mode="w") as fh:
            json.dump(log, fh, indent=2)
        return

    def updatelog_outfile(self, idx:str, fn_from:str, fn_to:str) -> None:
        """
        update the entry of `self.logdl` with key `idx`, repacing
        the filename `fn_from` by `fn_to`.

        in `self.fiximages()`, we normalize filenames and extensions,
        and thus sometimes rename a file that is present in `self.logdl`.
        in those cases, we need to update `self.logdl` with the new filename.
        else; the new file will be removed at the end of the pipeline with
        `self.cleanup()`, and we will have errors when rerunning this step,
        because we will target data that has been deleted.
        """
        idx = idx.strip()
        if os.path.isfile(self.logdl):
            with open(self.logdl, mode="r") as fh:
                log = json.load(fh)
            # get the index of `fn_from` in the list `log[idx]`
            # update it with the new index.
            if idx in log.keys() and fn_from in log[idx]:
                # print(idx, fn_from, fn_to, log[idx])
                pos = log[idx].index(fn_from)
                log[idx][pos] = fn_to

            elif idx in log.keys() and not len(log[idx]):
                log[idx].append(fn_to)

            else:
                raise ValueError(f"Iconography.updatelog_outfile(): can't update log with params idx:{idx}, fn_from:{fn_from}, fn_to:{fn_to} ; log entry: {log[idx]}")

            # then, update the file
            with open(self.logdl, mode="w") as fh:
                json.dump(log, fh, indent=2)
        return


    def isinlog_compressthumbnail( self, filename_full:str) -> bool:
        """
        check if an image has aldready been compressed
        and thumbnailed and is in `self.outimg`

        :param filename_full: the name of the filename to check
        :returns: true if the filename is in log
        """
        if os.path.isfile(self.logct):
            with open(self.logct, mode="r") as fh:
                log = json.load(fh)
                return filename_full.strip() in list(log.keys())
        return False


    def writelog_compressthumbnail( self
                                  , filename_full:str
                                  , filenames_ct:t.List[str]) -> None:
        """
        write a log entry:
        `{ <complete filename>: [<compressed and thumbnail versions of the same image>] }`

        :param filename_full : the name of the file that has been compressed/thumbnailed
        :param filenames_ct  : an array of the names of the compressed/thumbnailed files
        """
        if os.path.isfile(self.logct):
            with open(self.logct, mode="r") as fh:
                log = json.load(fh)
            log[filename_full] = filenames_ct
        else:
            log = { filename_full: filenames_ct }
        with open(self.logct, mode="w") as fh:
            json.dump(log, fh, indent=2)
        return


    def cleanup(self, df:DFType) -> DFType:
        """
        remove images in self.outimages that are
        not referenced in `self.df`
        """
        files = [ f for f in os.listdir(self.outimg)                  # all files in `self.outimg` (the `if` allows to filter out the directories)
                  if os.path.isfile(os.path.join(self.outimg, f)) ]
        tomove = [ f for f in files                                   # all files that are not referenced in dataframe
                   if f not in df.filenames.explode().unique() ]
        for f in tomove:
            os.rename( os.path.join(self.outimg, f)
                     , os.path.join(self.cleanupdir, f) )

        # for some obscure reason, some files in `df` are not in
        # `self.outimg` (2 out of 789) => delete them from `df`.
        # if the row ends up having no filename, delete the row altogether.
        pre = len(df.filenames.explode().loc[ df.filenames.explode().notna() ].to_list())        # number of file names before processing

        df.filenames = (df.filenames
                          .apply(lambda x: [ _ for _ in x if _ in files and not pd.isna(_) ]) )  # drop all filenames that are not in `files`
        df = df.loc[ df.filenames.apply(len).gt(0) ]                                             # drop rows with no filenames

        post = len(df.filenames.explode().loc[ df.filenames.explode().notna() ].to_list())       # number of filenames after processing

        print(f"* `Iconography.cleanup()`: deleted { pre-post } out of {pre} filenames that "
              + "are in the CSV but point to non-existent image files in `out/iconography/images`")

        assert all([ f in files for f in df.filenames.explode().unique() ]), \
               "not all filenames in the `iconography` df are in `self.outimg` !"
        return df

# def build_actors(self, df:DFType) -> DFType:
#     """
#     build a table containing the name of all `actors`:
#     publishers and authors, to do manual deduplication.

#     !!! WARNING !!!
#     the merging of `self.df_actors` and `df_actors_in` is not perfect
#     and can leave out a few rows. they must be completed by hand
#     before being used as an input for `2_data2sql`

#     :param df: the iconography dataframe
#     """
#     # 1) create the `df_actors`
#     self.df_actors.src = pd.concat([ df["author_1"], df["author_2"], df["publisher"] ]
#                                    , axis=0)
#     self.df_actors = (self.df_actors.explode(column="src")
#                                     .drop_duplicates(subset="src")
#                                     .dropna(subset="src"))
#     self.df_actors.src = (self.df_actors.src.str.replace(r"\s+", " ", regex=True)
#                                             .str.strip())
#     self.df_actors.date = self.df_actors.src.apply(date2range)

#     # do some quick simplification: drop rows that are more or less the same
#     self.df_actors["simp"] = (self.df_actors.src
#                                             .copy()
#                                             .apply(self.simplify))
#     self.df_actors = (self.df_actors.groupby(by="simp")
#                                     .agg(lambda x: x.iloc[0])
#                                     .reset_index()
#                                     .rename(columns={"index": "simp"}))
#     self.df_actors.index = pd.Index([ build_uuid() for _ in self.df_actors.index.to_list() ])

#     # 2) integrate previously done manual indexing of actors
#     # if `self.inactcsv` is a file, then we have aldready run `1_data_preparation`
#     # and have normalized names by hand. in that case, merge `self.df_actors` with
#     # `self.inactcsv` to keep the names that have aldready been normalized
#     try:
#         df_actors_in = read_df(self.inactcsv
#                                , column_names=["src", "first_name", "last_name", "date"]
#                                , useindexcol=0)

#         # pre-merge harmonisation: replace `df_actors_in.src` by the
#         # most similar `self.df_actors.src`.
#         # this is done by creating a column `similar` that contains,
#         # first the uuid of of the most resemblant row of `self.df_actors`,
#         # then the value of `self.df_actors.src` for that row. it removes
#         # up to 50% of the merging errors.
#         # * simplify `src` in `df_actors_in` to do comparisons
#         df_actors_in.src = (df_actors_in.src.str.replace(r"\s+", " ", regex=True)
#                                             .str.strip())
#         df_actors_in["simp"] = (df_actors_in.src
#                                             .copy()
#                                             .apply(self.simplify))
#         # * extract the most similar value of `self.df_actors_in.src` in `self.df_actors`
#         df_actors_in["similar"] = (df_actors_in.apply(lambda x: { name: rf_lev.normalized_similarity(x.simp, val)    # indexes of `self.df_actors` mapped to a levenshtein normalized similarity between `df_dup.src` and `self.df_actors.src`. 1: 100% similar, 0: 100% different
#                                                                   for name, val in self.df_actors.simp.items() }
#                                                       , axis=1)
#                                                .apply(lambda x: [ k for k, v in x.items()                            # if an index is mapped to uuids with scores > 0.9, extract the uuid with the highest score
#                                                                   if v > 0.9
#                                                                   and v == max( list(x.values()) )
#                                                                 ])
#                                                .apply(lambda x: x[0] if len(x) else np.nan)                          # convert 1-item list to scalar
#                                                .apply(lambda x: self.df_actors.loc[x].src                            # replace the uuid by the `src` in `self.df_actors_in`
#                                                                 if not pd.isna(x) else x))
#         # * perform the mapping
#         df_actors_in.src = df_actors_in.apply(lambda x: x.similar
#                                                         if not pd.isna(x.similar)
#                                                         else x.src
#                                               , axis=1)

#         self.df_actors = (pd.merge(self.df_actors, df_actors_in, how="outer"  # OUTER JOIN IS VERY IMPORTANT TO AVOID LOOSING DATA!!!
#                                    , on="src", suffixes=("_x", ""))
#                             .sort_values(axis=0, by="last_name", na_position="last"))
#         self.df_actors.date = self.df_actors.apply(lambda x: x.date_x
#                                                              if pd.isna(x.date)
#                                                              else x.date
#                                                    , axis=1)
#         self.df_actors = self.df_actors.drop(columns=[ "first_name_x", "last_name_x", "date_x"
#                                                      , "simp", "simp_x", "similar"])

#     # no `self.inactcsv` => no hand-normalized names to fetch => do nothing
#     except FileNotFoundError:
#         pass

#     # 3) finally, replace the integer index by an uuid to conform with other dfs
#     self.df_actors.index = [ build_uuid() for _ in range(self.df_actors.shape[0]) ]  # type: ignore

#     return df

# def col2canon(self, col:SType) -> SType:
#     """
#     `col` is a dataframe column containing arrays of strings. clusterize
#     those strings and replace non canonical terms with canonical ones,
#     i.e. the most used variant between similar strings.

#     warnings
#     ********
#     * this function is NON DETERMINISTIC and can produce different
#       results each time. it can really mess up a pipeline, most
#       importantly by breaking joins between tables
#     * this only works on list columns
#     """
#     vals     = []  # all unique items in our lists
#     transf   = {}  # dict mapping all values in `vals` to their canonical expression
#     simpmap  = {}  # dict of { <simplified term>: [<array of full terms>] }, later retyped to { <simplified term>: <canonical full term> }
#     simpvals = []  # array of simplified terms in vals
#     clusters = []  # variable to store clusterized values: [ [<array of similar names>], [<array of similar names>], ... ]

#     # 1) build `vals`, `simpvals` and `simpmap`
#     vals = [ v for v in col.explode().unique() if not pd.isna(v) ]
#     simpvals = [ self.simplify(v) for v in vals
#                  if self.simplify(v) not in simpvals ]
#     simpmap = { s:[] for s in simpvals }
#     for v in vals:
#         simpmap[ self.simplify(v) ].append(v)

#     # 2) process `simpmap`: extract the mode out of the array of full names
#     simpmap = { k:mode(v) for k,v in simpmap.items() }

#     # 3) clusterize alternate orthorgraphs for a single term in a list
#     for v in set(simpvals):
#         if len(clusters)==0 or not any(v in c for c in clusters):      # avoid computing the same cluster more than once
#             clusters.append([
#                 r[0] for r in rf.process.extract( v                    # `r[0]` = the matched string
#                                                 , simpvals
#                                                 , processor=None
#                                                 , score_cutoff=85)
#             ])
#     clusters = [ c for c in clusters if len(c) > 0 ]  # remove empty items

#     # 4) match each term in `vals` to their canonical form.
#     #    the canonical form is the most used alternate
#     #    orthograph for a term.
#     for v in simpvals:
#         # get the cluster that contains `v`
#         for c in clusters:
#             if v in c:
#                 cluster = c
#                 break
#         # extract a mode from `cluster`
#         mode_full = simpmap[mode(cluster)]
#         transf[v] = mode_full

#     # 5) update `df[col]` by replacing the terms by their canonical orthograph
#     col = ( col.apply(lambda x: [ self.simplify(_) for _ in x ])
#                .apply(lambda x: [ transf[_] for _ in x ]) )

#     return col


#    def input_missing(self, df:DFType) -> DFType:
#        " ""
#        update the dataframe's `filenames` column by adding file names
#        added manually in `self.inmiscsv`. the manually linked images will
#        be downloaded in `self.clean_imagefiles()`
#
#        in some cases, files can't be matched in `self.df`. for those files,
#        a csv `out/missing.csv` is created. the files must be
#        manually downloaded in `in/iconography/images` and the filenames added to
#        the pipe-seperated field `filenames` in `missing.csv`.
#        that file is then added as an input in `in/iconography`.
#        "" "
#        # read
#        df_missing = (read_df(self.inmiscsv, None, 1)      # structure: [ <id (index)>, <filenames array>, ... ]
#                     .drop(columns=["index"]))
#        df_missing = splitcell(df_missing, ["filenames"])
#
#        # insert filenames from `df_missing` in `df`
#        df.filenames.update(df_missing.filenames)
#        df["len"] = df.filenames.apply(len)
#        assert df.len.gt(0).sum() == df_missing.shape[0], \
#               "not all filenames in `df_missing` have been inserted in `df` !"
#
#       df = df.drop(columns=["len"])
#        return df

# def norm_idx_filepath(self, ro:SType) -> SType:
#     """
#     process each df row to normalize the ids and build
#     a path to the files related to a dataset entry. the
#     dataframe is left untouched by this step: data is
#     written to an aux file and will the df will be updated
#     once the df is updated
#
#     algorithm
#     *********
#     - if:
#       - there is an API to get images (ideally, a IIIF API)
#       - AND the row's identifier is "simple" (matches: "^\s*([A-Z]+)(\s|_)*([A-Z\d]+)\s*$",
#         , meaning that there is only one image for this row
#       - AND there is only one entry in the IIIF manifest's sequence
#         (meaning that there's only one image in the manifest
#       => redownload the image in full size and in thumbnail size.
#       save them in `out/`
#     - else (no api OR the row's ID points to several files OR there
#       are multiple images in the manifest and we can't determine
#       which one(s) we should use), get the images in the local
#       `in/iconography/images/` folder based on the entry's id.
#       save them in `out/`
#     - rename files: change their name by an UUID matching:
#       `qr1{UUID}(_thumbnail)?.{extension}`
#     - rename ids: change them to an UUID matching `qr1{UUID}`
#     - log all data
#
#     as a reminder:
#     **************
#     * a simple match is an id composed of:
#       `^{museum_code}{separator}?{hexadecimal_id}{separator}?{subid}?{separator}?$`
#       -> `MC 351`, `MC_351`, `MC351`, `MC 351-1`...
#     * a complex match is an id composed of:
#       `^{museum_code}{separator}?{hexadecimal_id}{separator}?{2 or more sub-ids}`
#       -> `MVH 6-1 et 2`, `MVH 9-1-2`, `MVH  11-1 à 3`...
#
#     logging
#     *******
#     downloading files from servers takes time. the processed entries
#     and matched filenames are logged in `self.logdl`. the dataframe
#     will be updated when 100% of the rows are processed.
#     `self.logdl` structure:
#     {
#         <old id>: [ <new uuid>, [<array of filenames>] ]
#     }
#
#     :param ro: a dataframe row
#     :returns: the dataframe with an extra row named `filename`
#     """
#     # ********************** FUNCTION DEFINITIONS ********************** #
#     def download_resource(self, url:str, extension:str) -> str:
#         """
#         download a file from requests and write it to `self.outimg`
#         we use `stream=True` to stream the file instead of downloading
#         it, thus limiting memory usage
#
#         on the BNF IIIF API, only 5 full size downloads/minute are
#         allowed.
#         see: https://gitlab.com/scripta/escriptorium/-/issues/807
#
#         :param url: the url to download the file from
#         :param str: the format to write the file to
#         :returns: the image name.
#         """
#         fname = f"{build_uuid()}.{extension}"
#         outfile = os.path.join(self.outimg, fname)
#
#         # avoid `429 too many requests` on BnF images: wait 20 seconds between requests
#         # between 2 bnf requests
#         if "bnf" in url and self.lastbnf is not None:
#             _ = (datetime.now() - self.lastbnf).total_seconds()  # time since the last bnf request in seconds
#             time.sleep(20 - _ if _ < 20 else 0)                  # make sure 20 seconds passed
#             self.lastbnf = datetime.now()                        # update the logger
#         elif "bnf" in url:
#             self.lastbnf = datetime.now()
#
#         # run request + dl image
#         with requests.get(url, stream=True) as r:
#             if r.status_code != 200:
#                 print(url)
#                 print(r.text)
#                 r.raise_for_status()
#             else:
#                 with open(outfile, mode="wb") as fh:
#                     for chunk in r:
#                         fh.write(chunk)
#         return fname
#
#     def dlfile(r: SType):
#         """
#         download a file using a (iiif) api
#         :param r: the row to process
#         """
#         # we're working with simple ids with a valid IIIF
#         # manifest => retrieve the data frm APIs
#         filenames = []
#         if not pd.isna(r.iiif_url):
#             url_img, format_img = iiif_extract(r.iiif_url)
#             # after processing the IIIF manifest, download the images
#             if url_img != "":
#                 filenames.append( download_resource(self, url_img, format_img) )
#         # there's a simple match + we have an API but
#         # no IIIF for this museum. no use case yet
#         else:
#             pass
#         return filenames
#
#     def mvfile(r:SType, idx_simple:str, idx_complex:str, museum_code:str):
#         """
#         match a file stored locally and move it to `out`
#         :param r: the row
#         :param idx_simple: normalized notation of the row's name if it is simple, `""` if not
#         :param idx_complex: array of normalized identifier if the index is complex, `[]` if not complex
#         :param museum_code: the museum that the row's index points to
#         """
#         filenames = []
#         # 1) we match the local files.
#         if ( idx_simple
#              and (isempty(r.iiif_url) or not hasapi(museum_code)) ):
#             # there's no manifest or API but we still have a simple match
#             filenames = idx2filename(idx_simple)
#         elif idx_complex:
#             # we have a complex match. there is or isn't an API available.
#             for i in idx_complex:
#                 for f in idx2filename(i):
#                     filenames.append(f)
#         # 2) we copy them to `OUT`
#         filenames = copy_resource(filenames, self.inimg, self.outimg)  # copy resources and return updates names
#         return filenames
#
#     # ********************** GLOBAL LOGIC ********************** #
#     idx = ro.name            # index of that row
#     filenames = []           # array our local filenames
#     idx_uuid = build_uuid()  # define the UUID for the current row in the dataset
#     _case = None
#
#     # skip rows that have aldready been processed
#     if self.isinlog(idx):  # type: ignore ;
#         return ro
#
#     elif not pd.isnull(idx):  # type: ignore ;
#         museum_code = ""  # identifier of the institution where the resource is located
#         _idx = idx        # save the former idx
#
#         # check if we have simple or complex
#         # matches and normalize index notation
#         idx = idx.strip()     # type: ignore ;
#         idx_simple = idx_simple_match(idx)     # normalized notation if simple, `""` if not simple
#         idx_complex = idx_complex_match(idx)   # array of normalized index if complex, `[]` if not complex
#
#         if idx_simple:
#             museum_code = re.search(r"^[A-Z]+", idx_simple)
#         elif idx_complex:
#             museum_code = re.search(r"^[A-Z]+", idx_complex[0])
#
#         # CASE 1 : we'll be retrieving images from distant files
#         if ( idx_simple
#              and (not pd.isnull(ro.iiif_url)  # there's a valid IIIF manifest
#                   or hasapi(museum_code)) ):      # type: ignore ; or there's a non-IIIF API
#             _case = 1
#             filenames = dlfile(ro)
#             if len(filenames) == 0:
#                 filenames = mvfile(ro, idx_simple, idx_complex, museum_code)  # type: ignore ; if no file has been downloaded, move it to `out/`
#
#         # CASE 2 : we're working with local files
#         else:
#             _case = 2
#             filenames = mvfile(ro, idx_simple, idx_complex, museum_code)  # type: ignore ;
#
#     if len(filenames) == 0:
#         pass
#         # print(f"* `norm_idx_filepath()`: no filename found for {idx}\n"
#         #       f"   * original id {_idx}\n"                                # type: ignore ;
#         #       f"   * case {_case}")
#
#     # log the processed idx, uuid and filenames to `self.logdl`
#     self.writelog(idx, idx_uuid, filenames)                               # type: ignore ;
#     return ro

#    @staticmethod
#    def norm_id_place(ro:SType) -> SType:
#        " ""
#        normalize the `id_place` column
#        " ""
#        norm_cell = []
#        rgx = r"([A-Z]+)\s*([A-Za-z0-9]*)"
#        for _ in ro.id_place:
#            idx = re.findall(rgx, _)
#            # simple case: only one place id in `_`
#            if len(idx) == 1:
#                norm_cell.append(f"{idx[0][0]}{idx[0][1]}")
#            elif len(idx) > 1:
#                # `RV1 à RV55` => add the range of ids
#                if re.search(rf"{rgx}\s*à\s*{rgx}", _):
#                    # build a range of street numbers and add them to `norm_cell`
#                    try:
#                        rng = [ int(idx[0][1]), int(idx[1][1])+1 ]
#                        for i in range(rng[0], rng[1]):
#                            norm_cell.append(f"{idx[0][0]}{i}")
#                    # typeerror if `int()` doesn't work. in that case,
#                    # we can't build a range and only add the 1st matched id
#                    # to `norm_cell`
#                    except TypeError:
#                        norm_cell.append(f"{idx[0][0]}{idx[0][1]}")
#                # in other cases, simply append the first matched id
#                else:
#                    norm_cell.append(f"{idx[0][0]}{idx[0][1]}")
#        ro.id_place = norm_cell
#        return ro
#
#    @staticmethod
#    def norm_url(ro: SType) -> SType:
#        " ""
#        normalize URLs, namely:
#        * ro.iiif_url (validate + correct + extract url)
#        * ro.source_url   (validate + cut the unecessary par that points
#          to a specific part of the html page + extract url)
#
#        :param ro: the csv row being processed
#        " ""
#        # normalize manifest
#        ro.iiif_url = iiif_url_match(ro.name, ro.iiif_url)  # type: ignore ;
#
#        # normalise resource url notation
#        if not isempty(ro.source_url):
#            ro.source_url = re.sub("#.+?$", "", ro.source_url)
#        else:
#            ro.source_url = np.nan
#
#        return ro


