import geopandas as gpd
import pandas as pd
import numpy as np
import os

from src.utils.constants import SHAPEFILES, OUT, OUT_VERIFIER, IN_CARTOGRAPHY, MUSEUM_CODES
from .utils.io import (read_df, copy_resource, write_df)
from .utils.strings import splitcell
from .iconography import Iconography
from .cartography import Cartography
from .place import Place
from .shared import Shared


# ************************************************
# run some pre-run verification to ensure
# that we are working with valid data
# ************************************************

class Verifier(Shared):
    """
    run some basic verifications before starting the cleaning
    process to ensure that there is at least a basic level of
    quality in our data
    """
    def __init__(self):
        self.isok                  = True  # flag that is switched to False if one of our verifications fail. if `True`, continue with the processing at the end of this step. if `False`, exit
        self.df_place              = Place().df
        self.out_icono_invalid     = os.path.join(OUT_VERIFIER, "icono_localisation_invalide.csv")
        self.out_icono_empty       = os.path.join(OUT_VERIFIER, "icono_localisation_manquante.csv")
        self.out_icono_institution = os.path.join(OUT_VERIFIER, "icono_institution_invalide.csv")
        self.out_carto_invalid     = os.path.join(OUT_VERIFIER, "carto_localisation_invalide.csv")
        self.out_carto_missing     = os.path.join(OUT_VERIFIER, "carto_geometrie_manquante.csv")
        self.out_loc_duplicates    = os.path.join(OUT_VERIFIER, "localisation_doublons.csv")
        return


    def pipeline(self):
        """
        verification pipeline
        """
        self.df_place = self.df_place.loc[ self.df_place.index.notna() ]
        self.df_place.index = self.df_place.index.map(lambda x: x.strip()
                                                            if isinstance(x, str) else x)

        self.verify_iconography().verify_cartography().verify_place()

        if not self.isok:
            print("")
            raise AssertionError("there are critical errors in the input. "
                                 "check `out/verifier/` for details and correct "
                                 "the errors in `in/` before starting again. exiting...")

        return self


    def verify_iconography(self):
        """
        verifiy that `df.id_place` point
        to place ids as defined in `df_place.index`.
        """
        # prepare the data
        df = Iconography().df
        df = splitcell(df, ["id_place"]).loc[ df.index.notna() ]  # type: ignore

        # 1) find the rows in `df` without a place id
        empty_icono = df.loc[ df.id_place.apply(len).eq(0) ].index.to_list()  # indexes of all rows in `df_iconography` with no place id
        if len(empty_icono):
            print(f"* {len(empty_icono)} rows in the iconography input CSV with no place identifier. add one or delete the row before continuing !")
            df_err = pd.DataFrame( empty_icono, columns=["identifiant iconographie"] )
            df_err.to_csv(self.out_icono_empty, sep="\t", index=False)
            df_err.to_excel(self.out_icono_empty.replace("csv", "xlsx"), index=False)

        # 2) find the rows with an invalid place id
        invalid_icono = [ [idx, row.id_place]                                 # array of [<df_iconography in row index>, <invalid_icono place id> ]
                          for idx,row in df.explode("id_place").iterrows()
                          if row.id_place not in self.df_place.index ]
        if len(invalid_icono):
            print(f"* {len(invalid_icono)} invalid place ids in the iconography input CSV.")
            df_err = pd.DataFrame( invalid_icono, columns=["identifiant iconographie", "identifiant de localisation invalide"])
            df_err.to_csv(self.out_icono_invalid, sep="\t", index=False)
            df_err.to_excel(self.out_icono_invalid.replace("csv", "xlsx"), index=False)

        # 3) check that for all rows,
        # ro.institution column points to one of MUSEUM_CODES.values()
        # OR the institution points to one of MUSEUM_CODES.keys()
        # OR the first part of ro.index points to one of `MUSEUM_CODES.keys()`
        mc_vals = [ v.lower() for v in MUSEUM_CODES.values() ]
        mc_keys = [ k.lower() for k in MUSEUM_CODES.keys() ]
        df["idxbase"] = df.index.to_series().str.extract(r"(^[A-Za-z]+)")
        df["institution2mc_vals"] = (df.institution.str.lower()
                                                   .str.strip()
                                                   .isin(mc_vals) )
        df["institution2mc_keys"] = (df.institution.str.lower()
                                                   .str.strip()
                                                   .isin(mc_keys) )
        df["idxbase2mc_keys"]     = df.idxbase.str.lower().isin(mc_keys)
        invalid_institution = df.loc[ df.idxbase2mc_keys.eq(False)
                                    & df.institution2mc_vals.eq(False)
                                    & df.institution2mc_keys.eq(False)
                                    , ["institution"] ]
        if invalid_institution.shape[0]:
            print(f"* {len(invalid_institution)} invalid institutions in the iconography"
                  " input CSV. change the identifier or the institution name to one of `MUSEUM_CODES`")
            df_err = pd.DataFrame(invalid_institution)
            df_err.to_csv(self.out_icono_institution, sep="\t", index=False)
            df_err.to_excel(self.out_icono_institution.replace("csv", "xlsx"), index=False)

        if any([ len(empty_icono), len(invalid_icono), len(invalid_institution) ]):
            self.isok = False

        return self


    def verify_cartography(self):
        """
        verifiy that:
        * the `id_loc` in our shapefile point to valid rows of `self.df_place`
        * every row of `df_place` is linked to at least 1 feature in a shapefile
        """
        # read the shapefiles into a dataframe with 2 columns:
        # `shapefile` (the source file) and `id_loc` (place id)
        reader = lambda x: gpd.read_file( os.path.join(IN_CARTOGRAPHY, "vector", x) )  # read a shapefile into a geodf
        mapper = []  # first: [{ "shapefile": <source shapefile>, "id_loc": [<array of place ids>] }]. then turned into a csv

        for s in SHAPEFILES:
            mapper.append({ "shapefile": s, "id_loc": reader(s).id_loc.to_list() })
        df = (pd.DataFrame(mapper).explode(column="id_loc")
                                  .dropna(subset=["id_loc"])
                                  .reset_index(drop=True))
        df.id_loc = df.id_loc.str.strip()

        # 1) all `id_loc` columns in the shapefiles point
        #    to a value in `self.df_place.index`
        df_invalid_loc = df.loc[ ~df.id_loc.isin(self.df_place.index) ]
        if df_invalid_loc.shape[0]:
            print(f"* {df_invalid_loc.shape[0]} `id_loc` values in the shapefile point to nothing in `df_place` !")
            df_invalid_loc.to_csv(self.out_carto_invalid, sep="\t", index=False)
            df_invalid_loc.to_excel(self.out_carto_invalid.replace("csv", "xlsx"), index=False)

        # 2) all place ids in `self.df_place` must point to a row in `df`
        df_missing_loc = self.df_place.loc[ ~self.df_place.index.isin(df.id_loc) ]
        if df_missing_loc.shape[0]:
            print(f"* {df_missing_loc.shape[0]} places in `df_place` with no matching features in the shapefiles !")
            df_missing_loc.to_csv(self.out_carto_missing, sep="\t", index=True)
            df_missing_loc.to_excel(self.out_carto_missing.replace("csv", "xlsx"), index=True)

        if df_missing_loc.shape[0] or df_invalid_loc.shape[0]:
            self.isok = False

        return self


    def verify_place(self):
        # 1) make sure that all place ids are unique
        duplicates = (self.df_place
                          .index.to_series()
                          .loc[ self.df_place.index.duplicated() ]
                          .to_list())
        if len(duplicates):
            print( f"* {len(duplicates)} duplicate place ids in "
                 + f"`in/place/place.csv`: `{duplicates}`")
            df_duplicates = pd.DataFrame(duplicates, columns=["identifiants de lieux en double"])
            df_duplicates.to_csv(self.out_loc_duplicates, sep="\t", index=True)
            df_duplicates.to_excel(self.out_loc_duplicates.replace("csv", "xlsx"), index=True)
            self.isok = False

        return self
