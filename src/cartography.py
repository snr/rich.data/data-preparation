from shapely import to_geojson as shapely_to_geojson
from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from osgeo import gdal, osr, ogr  # `ogr`: vectors + coordinate reference systems
import geopandas as gpd
from tqdm import tqdm
import pandas as pd
import numpy as np
import typing as t
import validators
import pyvips
import json
import os
import re


from .utils.constants import ( IN_CARTOGRAPHY, MUSEUM_CODES, OUT_CARTOGRAPHY
                             , IN_PLACE, TMP, PLACE_HEADERS
                             , CARTO_HEADERS, SHAPEFILES, CLEANUP)
from .utils.strings import build_uuid, date2range
from .utils.io import read_df, write_df


class Cartography():
    def __init__(self):
        tqdm.pandas()

        # useful directories
        self.tmp        = os.path.join(TMP, "cartography")
        self.ingis      = os.path.join(IN_CARTOGRAPHY, "gis")
        self.inrst      = os.path.join(IN_CARTOGRAPHY, "raster")
        self.invct      = os.path.join(IN_CARTOGRAPHY, "vector")
        self.outcsv     = os.path.join(OUT_CARTOGRAPHY, "cartography_clean.csv")
        self.outimg     = os.path.join(OUT_CARTOGRAPHY, "images")
        self.outrst     = os.path.join(OUT_CARTOGRAPHY, "raster")
        self.outvct     = os.path.join(OUT_CARTOGRAPHY, "vector")
        self.incsv      = os.path.join(IN_CARTOGRAPHY, "cartography.csv")
        self.cleanupdir = os.path.join(CLEANUP, "cartography")

        # dataframes
        self.df_md                        = read_df(self.incsv, CARTO_HEADERS, False)  # cartography metadata
        self.df_loc                       = read_df(os.path.join(IN_PLACE, "place.csv"), PLACE_HEADERS, 0)
        self.gdf_feuille, self.gdf_common = self.build_init_shapefiles()
        self.gdf                          = gpd.GeoDataFrame()  # concatenation of `self.gdf_feuille` and `self.gdf_common`. defined in `self.merge()`
        self.df                           = pd.DataFrame()      # pure pandas representation of `self.gdf`

        # ogr stuff
        self.proj_crs = osr.SpatialReference()  # WGS 84 as a projected coordinate system
        self.proj_crs.ImportFromWkt("""
            PROJCS["WGS 84 / Pseudo-Mercator",
                GEOGCS["WGS 84",
                    DATUM["WGS_1984",
                        SPHEROID["WGS 84",6378137,298.257223563,
                            AUTHORITY["EPSG","7030"]],
                        AUTHORITY["EPSG","6326"]],
                    PRIMEM["Greenwich",0,
                        AUTHORITY["EPSG","8901"]],
                    UNIT["degree",0.0174532925199433,
                        AUTHORITY["EPSG","9122"]],
                    AUTHORITY["EPSG","4326"]],
                PROJECTION["Mercator_1SP"],
                PARAMETER["central_meridian",0],
                PARAMETER["scale_factor",1],
                PARAMETER["false_easting",0],
                PARAMETER["false_northing",0],
                UNIT["metre",1,
                    AUTHORITY["EPSG","9001"]],
                AXIS["Easting",EAST],
                AXIS["Northing",NORTH],
                EXTENSION["PROJ4",
                    "+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 +k=1 +units=m +nadgrids=@null +wktext +no_defs"],
                AUTHORITY["EPSG","3857"]]
        """)
        self.geog_crs = osr.SpatialReference()  # WGS 84 as a geographical coordinate system
        self.geog_crs.ImportFromWkt("""
            GEOGCS["WGS 84",
                DATUM["WGS_1984",
                    SPHEROID["WGS 84",6378137,298.257223563,
                        AUTHORITY["EPSG","7030"]],
                    AUTHORITY["EPSG","6326"]],
                PRIMEM["Greenwich",0,
                    AUTHORITY["EPSG","8901"]],
                UNIT["degree",0.01745329251994328,
                    AUTHORITY["EPSG","9122"]],
                AUTHORITY["EPSG","4326"]]
        """)
        osr.SetPROJSearchPaths([ self.invct, self.inrst ])
        return


    def pipeline(self):
        """
        transform the shapefiles into a single dataframe,
        process the raster and vector files and save
        everything to `out/cartography`
        """
        (self.match_raster()
             .metadata()
             .merge()
             .index()
             .rasterprocess()
             .vectorprocess()
             .dateprocess()
             .institutionprocess()
             .urlprocess()
             .to_dataframe()
             .cleanup())

        write_df(self.df, self.outcsv)

        return self


    def build_init_shapefiles(self) -> t.List[DFType]:
        """
        populate the `self.gdf_feuille`, `self.gdf_common`
        shapefiles defined in `__init__()`

        all shapefiles (except the `Feuille-RueVivienne.shp`)
        must be concatenated into a single big geodataframe,
        `gdf_common`.
        `Feuille-RueVivienne.shp` is contained in a single gdf,
        `gdf_feuille`. the two are merged later on.
        """
        reader = lambda x: gpd.read_file( os.path.join(self.invct, x) )  # read a shapefile into a geodf
        gdfs = []
        cols = ["id_loc", "rich_uuid", "shp_source", "geometry"]
        for s in SHAPEFILES:
            if s == "Feuille-RueVivienne.shp":  # this one will have a specific processing
                gdf_feuille = reader(s).assign(source=s)
            else:
                gdf = reader(s).assign(shp_source=s)[cols]
                gdfs.append(gdf)
        gdf_common = (pd.concat(gdfs, axis=0, ignore_index=True)
                        .dropna(subset=["id_loc"])
                        .reset_index(drop=True))
        return [gdf_feuille, gdf_common]


    def match_raster(self):
        """
        """
        rasters = { re.sub(r"\.[^\.]+$", "", f):f    # { <filename without extension>: <filename> }
                    for f in os.listdir(self.inrst)
                    if f.endswith("tif") }

        self.gdf_common["raster"] = self.gdf_common.rich_uuid.map(rasters)
        return self


    def metadata(self):
        """
        inject metadata into the dataframes
        """
        def feuille_metadata(ro:SType) -> SType:
            """
            extract metadata from `df_md` based on the filename
            present in `gdf_feuille.Source`.
            """
            md = self.df_md.loc[ self.df_md.filenames.eq(ro.Source) ].squeeze()  # the matching row from `self.df_md`
            ro.institution      = md.institution
            ro.title            = f"Parcelles à la feuille: {md.description}"
            ro.date_source      = md.date_source
            ro.inventory_number = md.corpus
            ro.source_url       = md.source_url
            return ro

        # initialize metadata columns on `gdf_common` and `gdf_feuille`
        for k,v in self.__dict__.items():
            if k.startswith("gdf_"):
                v["institution"]      = ""
                v["title"]            = ""
                v["date_source"]      = ""
                v["inventory_number"] = ""
                v["source_url"]       = ""
                v["credits"]          = ""
                v["map_source"]       = ""  # the cartographic source (Vasserot, Cadastre 1900...). not to be confused with `Source`, pointing to a filename in `self.gdf_feuille`
                v["granularity"]      = ""  # precision granularity: parcelle, aile, galerie, empreinte (ensemble architectural)
                v["place"]            = ""  # what part of the neighbourhood is represented? (banque de france, palais royal...)
                self.__setattr__(k,v)

        # to insert metadata in the `gdf_feuille`, match with rows
        # from `self.df_md`, containing metadata on polygons+rasters
        self.gdf_feuille = self.gdf_feuille.apply(feuille_metadata, axis=1)

        # inject metadata based on source for the other rows
        mask = self.gdf_common.shp_source.str.contains("Vasserot")
        self.gdf_common.loc[mask, "institution"]      = "Archives Nationales"
        self.gdf_common.loc[mask, "title"]            = "Atlas Vasserot"
        self.gdf_common.loc[mask, "date_source"]      = "1810|1836"
        self.gdf_common.loc[mask, "inventory_number"] = "F/31/75 et F/31/76"
        self.gdf_common.loc[mask, "source_url"]       = "https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/FRAN_IR_059652"
        self.gdf_common.loc[mask, "credits"]          = "" # tbd

        mask = self.gdf_common.shp_source.str.contains("Mosa")
        self.gdf_common.loc[mask, "institution"]      = "Archives de Paris"
        self.gdf_common.loc[mask, "title"]            = "Plan parcellaire municipal de Paris"
        self.gdf_common.loc[mask, "date_source"]      = "1860|1900"
        self.gdf_common.loc[mask, "inventory_number"] = "PP/11860 et PP/11870"
        self.gdf_common.loc[mask, "source_url"]       = "https://archives.paris.fr/f/planspacellaires/tableau/?&reset_facette=1&crit1=9&v_9_1=Paris+dans+ses+limites+%E0+partir+de+1860"  # no inventory found
        self.gdf_common.loc[mask, "credits"]          = "" # tbd

        mask = self.gdf_common.shp_source.str.contains("Billaud")
        self.gdf_common.loc[mask, "institution"]      = "Paris Musées"
        self.gdf_common.loc[mask, "title"]            = "Plan des Galeries et Rotonde Colbert / Conduisant de la rue Vivienne à la Rue Neuve des Petits Champs et au Palais Royal par le passage des Pavillons"
        self.gdf_common.loc[mask, "date_source"]      = "sans date"
        self.gdf_common.loc[mask, "inventory_number"] = ""
        self.gdf_common.loc[mask, "source_url"]       = "https://www.parismuseescollections.paris.fr/fr/musee-carnavalet/oeuvres/plan-des-galeries-et-rotonde-colbert-conduisant-de-la-rue-vivienne-a-la-0"
        self.gdf_common.loc[mask, "credits"]          = "" # tbd

        mask = self.gdf_common.shp_source.str.contains("GalerieVivienne")
        s = self.gdf_feuille.loc[ self.gdf_feuille.id_loc.eq("GV0") ].squeeze()  # `Feuille-GalerieVivienne` is derived from `GV0` in `Feuille-RueVivienne.shp`
        self.gdf_common.loc[mask, "institution"]      = s.institution
        self.gdf_common.loc[mask, "source_url"]       = s.source_url
        self.gdf_common.loc[mask, "title"]            = s.title
        self.gdf_common.loc[mask, "date_source"]      = s.date_source
        self.gdf_common.loc[mask, "inventory_number"] = s.inventory_number
        self.gdf_common.loc[mask, "credits"]          = "" # tbd

        # add info on the places
        mask = self.gdf_common.shp_source.str.contains("PalaisRoyal")
        self.gdf_common.loc[mask, "place"] = "Palais Royal"
        mask = self.gdf_common.shp_source.str.contains("(Vivienne|Colbert)", regex=True)
        self.gdf_common.loc[mask, "place"] = "Rue Vivienne"
        mask = self.gdf_common.shp_source.str.contains("BanqueDeFrance")
        self.gdf_common.loc[mask, "place"] = "Banque de France"

        # finally, add 3 columns:
        # * map_source, containing the global source of the row:
        #   - vasserot
        #   - parcellaire1900
        #   - feuille
        #   - billaud
        #   - contemporain for contemporary cartographic sources
        # * granularity , describing the precision of the row
        #   - parcelle, galerie
        #   - parcelle/aile/monument for the Palais-Royal
        #   - point for the full street points)
        # this is dependent on the name of the shapefiles so avoid renaming them
        # + change the below code when adding new cases and shapefiles
        mask = lambda x: self.gdf_common.shp_source.str.contains(x)  # select the rows in which `shp_source` contains the string 'x'
        self.gdf_feuille.map_source  = "feuille"
        self.gdf_feuille.granularity = "parcelle"
        self.gdf_common.loc[ mask("Mosa"), "map_source" ]                   = "parcellaire1900"
        self.gdf_common.loc[ mask("Feuille"), "map_source" ]                = "feuille"
        self.gdf_common.loc[ mask("Billaud"), "map_source" ]                = "billaud"
        self.gdf_common.loc[ mask("Vasserot"), "map_source" ]               = "vasserot"
        self.gdf_common.loc[ mask("Points"), "map_source" ]                 = "contemporain"

        self.gdf_common.loc[ mask("BanqueDeFrance"), "granularity" ]        = "parcelle"
        self.gdf_common.loc[ mask("PalaisRoyal-Ailes"), "granularity" ]     = "aile"
        self.gdf_common.loc[ mask("PalaisRoyal-Empreinte"), "granularity" ] = "ensemble"
        self.gdf_common.loc[ mask("PalaisRoyal-Parcelles"), "granularity" ] = "parcelle"
        self.gdf_common.loc[ mask("RueVivienne"), "granularity" ]           = "parcelle"
        self.gdf_common.loc[ mask("GalerieVivienne"), "granularity" ]       = "galerie"
        self.gdf_common.loc[ mask("GalerieColbert"), "granularity" ]        = "galerie"
        self.gdf_common.loc[ mask("Points"), "granularity" ]                = "point"

        assert all( self.gdf_feuille.Source.isin(self.df_md.filenames) ), \
               "Cartography.metadata(): not all rows of `self.gdf_feuille` point to a row in `self.df_md` !"
        assert self.gdf_feuille["map_source"].eq("").sum() == 0, \
               f"Cartography.metadata(): `gdf_feuille.map_source` has {self.gdf_feuille['map_source'].eq('').sum()} missing values"
        assert self.gdf_feuille["granularity"].eq("").sum() == 0, \
               f"Cartography.metadata(): `gdf_feuille.granularity` has {self.gdf_feuille['granularity'].eq('').sum()} missing values"
        assert self.gdf_common.map_source.eq("").sum() == 0, \
               f"Cartography.metadata(): `gdf_common.map_source` has {self.gdf_common.map_source.eq('').sum()} missing values"
        assert self.gdf_common.granularity.eq("").sum() == 0, \
               f"Cartography.metadata(): `gdf_common.granularity` has {self.gdf_common.granularity.eq('').sum()} missing values"
        return self


    def merge(self):
        """
        merge `self.gdf_common` with `self.gdf_feuille`
        """
        cols = [ "id_loc", "title", "date_source", "inventory_number"
               , "source_url", "credits", "map_source", "granularity"
               , "geometry", "raster", "institution"]
        self.gdf = pd.concat( [self.gdf_common[cols], self.gdf_feuille[cols]]
                            , ignore_index=True, axis=0)

        assert all(self.gdf.id_loc.ne("")), \
               f"Cartography.merge(): `self.gdf.id_loc` has {self.gdf.id_loc.ne('')} missing values"
        assert all(self.gdf.raster.ne("")), \
               f"Cartography.merge(): `self.gdf.raster` has {self.gdf.raster.ne('')} missing values"
        return self


    def index(self):
        """
        replace `self.gdf.index` by UUIDs
        """
        self.gdf.index = pd.Index([ build_uuid()                              # type:ignore
                                    for _
                                    in self.gdf.index.to_series().items() ])  # type:ignore
        return self


    def rasterprocess(self):
        """
        process the raster images with `df.hasraster == True`:
        * convert tif to png
        * transform the outline to black
        * copy found raster files to `self.outrst`

        about EPSG projection in Leaflet:
        * https://epsg.io/3857
        * https://leafletjs.com/reference-1.4.0.html#crs-l-crs-epsg3857
        """
        # (the function's logic is at the bottom, after
        # defining nested functions and other useful stuff)

        # gdal stuff:
        png_driver = gdal.GetDriverByName("PNG")  # gdal driver to translate geotif to png
        transform  = osr.CoordinateTransformation(self.proj_crs, self.geog_crs)  # transformation from projected WGS 84 to geographical WGS 84
        epsg       = f"epsg{ self.proj_crs.GetAttrValue('AUTHORITY', 1) }"  # epsg code

        def tifreproj(ro:SType) -> SType:
            """
            convert a tiff file to png and save it
            """
            ds = gdal.OpenEx(os.path.join(self.inrst, ro.raster)  # `ds` = dataset
                             , gdal.GA_ReadOnly)
            # reproject the geotiff, from `EPSG:3949` to
            # `EPSG:3857`, used by default in Leaflet
            ro.raster = re.sub(r"\.[^\.]+?$", "", ro.raster) + f"_{epsg}.tif"
            gdal.Warp( os.path.join(self.tmp, ro.raster)  # output
                     , ds                                 # dataset to transform
                     , options=gdal.WarpOptions( format="GTiff"
                                               , dstSRS="EPSG:3857"
                                               , width=ds.RasterXSize
                                               , height=ds.RasterYSize)
            )
            del ds  # datasets must be closed explicitly

            # extract latitude and longitude bounds (used to position the
            # png it in leaflet) and save them as leaflet LatLngBounds:
            # (https://leafletjs.com/reference.html#latlngbounds)
            ds = gdal.OpenEx(os.path.join(self.tmp, ro.raster)
                             , gdal.GA_ReadOnly)
            gt = ds.GetGeoTransform()
            # get bounds in the geotiff's projected crs: each point is expressed as x/y coordinates
            xmin = gt[0]                                                # western bound
            ymin = gt[3] + ds.RasterXSize*gt[4] + ds.RasterYSize*gt[5]  # northern bound
            xmax = gt[0] + ds.RasterXSize*gt[1] + ds.RasterYSize*gt[2]  # eastern bound
            ymax = gt[3]                                                # southern bound
            # convert the bounds to a geographical crs to extract latlng
            xmin,ymin,xmax,ymax = transform.TransformBounds(xmin, ymin, xmax, ymax, 21)
            ro.latlngbounds = [ [xmin,ymin], [xmax,ymax] ]  # convert to a leaflet-type latlngbounds

            # open the reprojected file + convert it to `png` + save it to `self.tmp`
            ro.raster = ro.raster.replace(".tif", ".png")  # update the extension
            png_driver.CreateCopy( os.path.join(self.tmp, ro.raster)
                                 , ds
                                 , strict=0)
            del ds
            return ro


        def jpgreproj(ro:SType) -> SType:
            """
            so far, none of our jpgs have projection info (an `.aux.xml` file).
            in turn, we don't reproject and simply copy the files to `self.tmp`
            """
            if not pd.isna(ro.aux):
                raise NotImplementedError(f"row `{ro.name}` has a jpg file and an aux file: `{ro.aux}`. "
                                          +"jpg reprojection not yet implemented. exiting...")
            else:
                pass # shutil.copy2(self.gisfiles[ro.raster], os.path.join(self.tmp, ro.raster))
            return ro


        def black2alpha(ro:SType) -> SType:
            """
            transform a color to alpha (mimics GIMP's color to alpha)
            implemented from: https://stackoverflow.com/questions/55582117/efficiently-converting-color-to-transparency-in-python/55629222#55629222

            :param infile: the input filename. this is the basename, without the full path
            """
            img = pyvips.Image.new_from_file( os.path.join(self.tmp, ro.raster)
                                            , access="sequential")
            if re.search("_epsg.+?$", ro.raster):
                ro.raster = build_uuid() + re.search("_epsg.+?$", ro.raster)[0]
            else:
                ro.raster = build_uuid() + re.search("\.[^\.]+$", ro.raster)[0]

            # the image aldready has an alpha layer => only write to the output
            if img.bands == 4:
                # print(f"{ro.raster} aldready in RBGA. not creating an alpha layer.")
                img.write_to_file(os.path.join(self.outrst, ro.raster))
                return ro

            tgt = [0,0,0]                                     # target color: black in rgb
            tol = 1                                           # distance to black in rgb: how close to the target color (1..100 i think)
            dst = sum(((img - tgt) ** 2).bandsplit()) ** 0.5  # distance to `tgt` of each pixel of `i`, as a pyvips band
            alpha = 255 * dst / tol                           # create an alpha layer

            # add the alpha band to `img` to convert it to RGBA + write to file
            img.bandjoin(alpha).write_to_file(os.path.join( self.outrst, ro.raster))
            return ro

        # pipeline
        self.gdf["latlngbounds"] = np.nan
        # gdf = self.gdf.loc[ self.gdf.isfullstreet == False ].dropna(subset=["raster"])  # df without empty rows in `df.raster`- + without the row describing `Parcelles-Vivienne`
        gdf = self.gdf.dropna(subset=["raster"])                       # df without empty rows in `df.raster`
        gdf = gdf.assign(ext=gdf.raster.str.extract(r"\.([^\.]+?)$"))  # save file extension in another column named `ext`

        gdf_tif = gdf.loc[ gdf.ext == "tif" ]                          # df with rows with tif raster files
        gdf_jpg = gdf.loc[ (gdf.ext.notna()) & (gdf.ext!="tif") ]      # df with rows with all non-tif raster files
        gdf_tif = gdf_tif.progress_apply(tifreproj, axis=1)
        gdf_jpg = gdf_jpg.progress_apply(jpgreproj, axis=1)

        gdf.update(gdf_tif)
        gdf.update(gdf_jpg)
        gdf = gdf.progress_apply(black2alpha, axis=1)

        # update the df
        self.gdf.update(gdf)
        return self


    def vectorprocess(self):
        """
        reproject the vectors + save them to
        geojson in the `self.gdf.vector` column

        geopandas offers a `GeoSeries.jo_json()` to
        convert a geometry to JSON, but it raises an
        `OverflowError: Maximum recursion level reached`.
        we have to use shapely instead.

        as is, our vector column contains only the `geometry`
        part of a GeoJSON. to be a valid GeoJson, it must be
        enclosed in:
        {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "properties": {},
               "geometry": <OUR VECTOR COLUMN>
            },
            # other features could be added here
          ]
        }
        """
        self.gdf = self.gdf.to_crs(self.geog_crs.ExportToWkt())
        self.gdf["vector"] = (self.gdf.geometry
                                      .remove_repeated_points(tolerance=0.0)
                                      .apply(shapely_to_geojson, indent=2)
                                      .apply(json.loads) )
        # self.gdf.vector = self.gdf.vector.remove_repeated_points(tolerance=0.0)
        return self


    def dateprocess(self):
        """
        convert the dates in `self.gdf.date_source`
        to an integer range and save it to `self.gdf.date`
        """
        self.gdf["date"] = (self.gdf.date_source
                                    .str.lower()
                                    .replace(r"sans\s+date", np.nan, regex=True)
                                    .apply(lambda x: date2range(x)
                                                     if not pd.isna(x)
                                                     else x))
        return self


    def institutionprocess(self):
        """
        normalize the notation of the `institutions` column
        this step raises pandas SettingWithCopyWarning warnings, but after looking well it doesn't cause errors.
        """
        simplify = lambda x: x.lower().strip()

        gdf = self.gdf.loc[ self.gdf.institution.apply(len).gt(0) ]
        mc_keys = [ simplify(_) for _ in MUSEUM_CODES.keys() ]
        mc_vals = [ simplify(_) for _ in MUSEUM_CODES.values() ]
        mc_keys_mapper = { simplify(k):v for k,v in MUSEUM_CODES.items() }  # simplified institution code to full institution name
        mc_vals_mapper = { simplify(v):v for v in MUSEUM_CODES.values() }   # simplified institution name to full institution name

        # `True` in any of these columns means that an institution
        #  name can be retrieved from `gdf.institution`
        gdf["institution2mc_keys"] = gdf.institution.apply(simplify).isin(mc_keys)
        gdf["institution2mc_vals"] = gdf.institution.apply(simplify).isin(mc_vals)

        # `notin` is an array of all items in `self.gdf.institution`
        # that can't be linked to `MUSEUM_CODES`. if it contains items,
        # raise an ArrertionError. this should be done in `verifier` but
        # it's harder because creating `self.gdf` is complicated
        notin = gdf.loc[ gdf.institution2mc_keys.eq(0)     # the simplified institution name in the row isn't in `mc_keys`
                       & gdf.institution2mc_vals.eq(0)     # the simplified institution name in the row isn't in `mc_vals`
                       , "institution" ].unique()          # retrieve all unique values for `self.gdf.institution` that don't satisfy the above
        if len(notin):
            raise AssertionError( "Cartography.institutionprocess(): "
                                + f"{len(notin)} values of self.gdf not in MUSEUM_CODES: "
                                + str(notin))

        # else, normalise the notation of `self.gdf.instition`
        gdf["institution_clean"] = ""
        gdf.loc[ gdf.institution2mc_keys, "institution_clean" ] = (gdf.loc[ gdf.institution2mc_keys, "institution" ]
                                                                      .apply(simplify)
                                                                      .map(mc_keys_mapper))
        gdf.loc[ gdf.institution2mc_vals, "institution_clean" ] = (gdf.loc[ gdf.institution2mc_vals, "institution" ]
                                                                      .apply(simplify)
                                                                      .map(mc_vals_mapper))

        gdf.institution = gdf.institution_clean
        gdf = gdf.drop(columns=["institution_clean", "institution2mc_vals", "institution2mc_keys"])
        self.gdf.update(gdf)
        return self


    def urlprocess(self):
        """
        clean the `source_url` and `url_image`
        cols: remove invalid urls
        """
        self.gdf.source_url = (self.gdf.source_url
                                   .str.strip()
                                   .apply(lambda x: x if validators.url(x) else np.nan))
        return self


    def to_dataframe(self):
        """
        convert the geodataframe to a pure-pandas df.
        geometry info is stored in `vector`, the CRS
        is stored in `crs_wkt` and `crs.epsg` columns
        """
        self.df = pd.DataFrame(self.gdf.drop(columns="geometry"))
        self.df["crs_wkt"] = self.gdf.crs
        self.df["crs_epsg"] = self.gdf.crs.to_epsg()
        self.df = self.df.rename(columns={ "id_loc": "id_place" })  # for conformity with step 2_data2sql

        del self.gdf  # avoid using `self.gdf` after creating `self.df`

        return self


    def cleanup(self):
        """
        move unecessary files (not linked to `self.df`) to `self.cleanupdir`
        """
        # move the files to `self.cleanupdir`
        outrst = [ f for f in os.listdir(self.outrst) ]
        tomove = [ _ for _ in outrst
                   if _ not in self.df.loc[self.df.raster.notna(), "raster"].explode().to_list() ]

        for t in tomove:
            os.rename( os.path.join(self.outrst, t)
                     , os.path.join(self.cleanupdir, t))

        # make sure that all files in `self.df.raster` are in `self.outrst`
        outrst = [ f for f in os.listdir(self.outrst) ]
        notin = [ _ for _ in self.df.loc[self.df.raster.notna(), "raster"].explode()
                  if _ not in outrst ]
        assert not len(notin), \
               f"Cartography.cleanup(): {len(notin)} entries in `self.df.raster` are not present in `{self.outrst}`"
        return self


