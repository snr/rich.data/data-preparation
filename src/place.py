from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from tqdm import tqdm
import pandas as pd
import numpy as np
import os

from .utils.constants import (IN_PLACE, OUT_PLACE, PLACE_HEADERS)
from .utils.io import read_df, write_df
from .utils.strings import build_uuid
from .shared import Shared


# ********************************************************************************
# clean and process the place dataset to
# prepare it for web publishing
# ********************************************************************************


class Place(Shared):
    """
    class containing the pipeline and necessary functions
    for the place dataset's processing.
    """
    def __init__(self):
        tqdm.pandas()
        self.incsv = os.path.join(IN_PLACE, "localisations_viviennepalaisroyalbanque_20240719.csv")
        self.outcsv = os.path.join(OUT_PLACE, "place_clean.csv")
        # self.df = read_df(self.incsv, PLACE_HEADERS, useindexcol=0)
        self.df = pd.read_csv( self.incsv, sep="\t", quotechar='"'
                             , header=0, skip_blank_lines=True
                             , names=PLACE_HEADERS, index_col=False
                             ).set_index("index")

    def pipeline(self):
        """
        cleaning pipeline
        """
        rgx = r"(v[eé]rifier|^x$|absent)"
        self.df = (self.df.loc[ self.df.index.notnull() ]
                          .replace(r"^\s*$", np.nan, regex=True))

        mask = lambda x: self.df[x].str.lower().str.contains(rgx, na=False, regex=True)  # `x` = column name
        self.df.loc[ mask("current"), "current" ]     = np.nan
        self.df.loc[ mask("plot1820"), "plot1820" ]   = np.nan
        self.df.loc[ mask("vasserot"), "vasserot" ]   = np.nan
        self.df.loc[ mask("atlas1860"), "atlas1860" ] = np.nan
        self.df["id_richelieu"] = self.df.index
        self.df.index = self.df.index.map({ i:build_uuid() for i in self.df.index.to_list() })  # replace the `index` by an uuid

        write_df(self.df, self.outcsv)
        return


    # def cleanup(self, df:DFType) -> DFType:
    #     """
    #     remove the image files that are not mentionned in `df`
    #     """
    #     print("""
    #           ########################################
    #           # Place.cleanup() TODOOOOOOOOOOOOOO #
    #           #            (if necessary)            #
    #           ########################################
    #     """)
    #     return df


