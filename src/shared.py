from typing import Callable
from tqdm import tqdm


class Shared():
    """
    parent class for all `Iconography`|`Cartography` classes 
    that holds functions used by several child classes
    """ 
    def _progress_apply(
            self
            , func:Callable
            , axis:int=1
            , instancefunc:bool=False
            , **kwargs):
        """
        wrapper for `df.progress_apply` that allows us to pipe 
        applies with an `IconographyProcesses` object.
        
        currently, the `**kwargs` are all passed to `func`, and 
        cannot be used as arguments of `.pipe`
        
        :param func: the function to pass
        :param axis: the axis of the `df.apply`
        :param instancefunc: wether the function is an instance function 
                             (in a class, with `self`) or not
        :param kwargs:any other arguments to be passed to `df.apply`
        """
        tqdm.pandas()
        if instancefunc:
            self.df = self.df.progress_apply(lambda x: func(self, x, **kwargs), axis=axis)
        else:
            self.df = self.df.progress_apply(func, axis=axis, **kwargs)
        return self
    
    
    def _pipe(self, func:Callable, instancefunc:bool=False, **kwargs):
        """
        wrapper for `df.pipe` that allows us to pipe `.pipe`
        with an `Iconography`|`Cartography` object.
        
        since `df.pipe()` passes the `df` to the inner function,
        the dataframe NEEDS TO BE PASSED EXPLICITLY to `func`,
        even if it may be accessed through the `self`, in an
        instance function.
        
        currently, the `**kwargs` are all passed to `func`, and 
        cannot be used as arguments of `.pipe`
        
        :param func: the function to pass
        :param instancefunc: wether the function is an instance function 
                             (in a class, with `self`) or not
        :param kwargs: any other arguments to be passed to `df.pipe`
        """
        if instancefunc:
            self.df = self.df.pipe(lambda x: func(self, x, **kwargs))
        else:
            self.df = self.df.pipe(func, **kwargs)
        return self

