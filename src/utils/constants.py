import os


# ****************************************************
# all our constants are here
# ****************************************************


# paths
UTILS           = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))
ROOT            = os.path.abspath(os.path.join(UTILS, os.pardir, os.pardir))
SRC             = os.path.abspath(os.path.join(UTILS, os.pardir))
TMP             = os.path.abspath(os.path.join(SRC, "tmp"))
LOGS            = os.path.abspath(os.path.join(SRC, "logs"))
OUT             = os.path.abspath(os.path.join(ROOT, "out"))
IN              = os.path.abspath(os.path.join(ROOT, "in"))
IN_PLACE        = os.path.abspath(os.path.join(IN, "place"))
OUT_PLACE       = os.path.abspath(os.path.join(OUT, "place"))
IN_CARTOGRAPHY  = os.path.abspath(os.path.join(IN, "cartography"))
IN_ICONOGRAPHY  = os.path.abspath(os.path.join(IN, "iconography"))
OUT_ICONOGRAPHY = os.path.abspath(os.path.join(OUT, "iconography"))
OUT_CARTOGRAPHY = os.path.abspath(os.path.join(OUT, "cartography"))
OUT_VERIFIER    = os.path.abspath(os.path.join(OUT, "verifier"))
CLEANUP         = os.path.abspath(os.path.join(ROOT, "cleanup"))
IN_TO_COMPLETE  = os.path.abspath(os.path.join(LOGS, "to_complete_in"))
OUT_TO_COMPLETE = os.path.abspath(os.path.join(LOGS, "to_complete_out"))

# name of shapefiles to process
SHAPEFILES = [ "Billaud-GalerieColbert.shp"
             , "Feuille-GalerieVivienne.shp"
             , "Feuille-RueVivienne.shp"
             , "Mosa-BanqueDeFrance.shp"
             , "Mosa-PalaisRoyal-Ailes.shp"
             , "Mosa-PalaisRoyal-Empreinte.shp"
             , "Mosa-PalaisRoyal-Parcelles.shp"
             , "Mosa-RueVivienne.shp"
             , "Rues-Points.shp"
             , "Vasserot-BanqueDeFrance.shp"
             , "Vasserot-PalaisRoyal-Ailes.shp"
             , "Vasserot-PalaisRoyal-Empreinte.shp"
             , "Vasserot-PalaisRoyal-Parcelles.shp"
             , "Vasserot-RueVivienne.shp" ]

# column headers for pandas. like the bindings,
# column headers are dated and mapped to a file in `in/`
ICONO_HEADERS = [
  "institution"
  , "index"
  , "iiif_url"
  , "iiif_folio"
  , "source_url"
  , "title"
  , "title_secondary"
  , "author_1"
  , "author_2"
  , "publisher"
  , "date_source"
  , "date_corr"
  , "technique"
  , "inventory_number"
  , "corpus"
  , "description"
  , "inscription"
  , "commentary"
  , "theme"
  , "named_entity"
  , "id_place"
  , "internal_note"
  , "produced_richelieu"
  , "represents_richelieu"
]

CARTO_HEADERS = [
    "institution"
    , "filenames"
    , "description"
    , "address"
    , "id_place"
    , "corpus"
    , "source_url"
    , "date_source"
    , "internal_note"
    , "url_image"
]

CARTO_LOC_HEADERS = [
    "institution"
    , "filenames"
    , "description"
    , "address"
    , "corpus"
    , "source_url"
    , "date_source"
    , "id_place"
    , "url_image"
    , "date"
    , "commentaire"
    , "isfullstreet"
    , "raster"
    , "aux"
    , "vector"
    , "latlngbounds"
]

PLACE_HEADERS = [
    "index"        # "identifiant"
    , "current"    # "adresse actuelle",
    , "atlas1860"  # "Atlas 1860- 1900"
    , "plot1820"   # "Parcelles à la feuille vers 1820-1850"
    , "vasserot"   # "Atlas Vasserot (1810-1836)";
]


# modes to define different pipelines/processings of the datasets
# MODES = ["icono", "carto"]

# museum identifiers / full name
MUSEUM_CODES = {
    # paris musées
    "MC"     : "Musée Carnavalet (Paris Musées)",
    "PG"     : "Palais Galliera (Paris Musées)",
    "MMB"    : "Maison de Balzac (Paris Musées)",
    "MVH"    : "Maison Victor Hugo (Paris Musées)",
    "MVR"    : "Musée de la Vie Romantique (Paris Musées)",
    "PP"     : "Petit Palais. Musée des Beaux-Arts de la Ville de Paris (Paris Musées)",
    "PM"     : "Paris Musées",
    "MB"     : "Musée Bourdelle (Paris Musées)",
    "ML"     : "Musée de la Libération Leclerc Moulin (Paris Musées)",

    # bibliothèques spécialisées de la ville de Paris
    "BHVP"   : "Bibliothèque historique de la Ville de Paris (Bibliothèques spécialisées de la Ville de Paris)",
    "BHD"    : "Bibliothèque de l'Hôtel de Ville (Bibliothèques spécialisées de la Ville de Paris)",
    "BD"     : "Bibliothèque Marguerite Durand (Bibliothèques spécialisées de la Ville de Paris)",
    "F"      : "Bibliothèque Forney (Bibliothèques spécialisées de la Ville de Paris)",
    "MMP"    : "Médiathèque musicale de Paris (Bibliothèques spécialisées de la Ville de Paris)",
    "BILIPO" : "Bibliothèque des littératures policières (Bibliothèques spécialisées de la Ville de Paris)",
    "BTV"    : "Bibliothèque du tourisme et des voyages - Germaine Tillion (Bibliothèques spécialisées de la Ville de Paris)",

    # autres musées et institutions importantes
    "BNF"    : "Bibliothèque nationale de France",
    "BM"     : "British Museum",
    "CVP"    : "Comission du vieux Paris",
    "INHA"   : "Institut national d'histoire de l'art",
    "MAK"    : "Musée Albert Kahn",

    # archives
    "AN"     : "Archives nationales",
    "AP"     : "Archives de Paris",

    # autres bibliothèques municipales
    "MD"     : "Médiathèque Marguerite Duras",
    "BMR"    : "Bibliothèque municipale et partimoniale Villon"
}

# indicating if there are available APIs (other than IIIF,
# which is indicated in another column)  for these museums.
# `True` if they exist, `False` if they don't, `None`
# if I don't know
MUSEUM_API = {
    "MC": True,
    "PG": True,
    "PP": True,
    "MMB": True,
    "MVH": True,
    "BNF": True,

    "CVP": None,    # à vérifier, probablement non
    "INHA": None,   # à vérifier, probablement oui
    "BHD": None,    # à vérifier
    "MD": None,     # idk

    "BM": False,    # à vérifier
    "BHVP": False,  # non, sauf si on arrive à plier leur game
}
