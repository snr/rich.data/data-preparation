from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
import pandas as pd
import typing as t
import shutil
import json
import sys
import re
import os

from .constants import ( OUT, OUT_ICONOGRAPHY, OUT_PLACE, OUT_VERIFIER
                       , OUT_CARTOGRAPHY, LOGS, TMP, CLEANUP, IN_TO_COMPLETE
                       , OUT_TO_COMPLETE)
from .strings import build_uuid



# ********************************************************
# reading, writing and moving files
# ********************************************************



def read_df( infile:str
           , column_names:t.Optional[t.List]
           , useindexcol:bool|int) -> DFType:
    """
    read a csv file as a dataframe.

    * if `useindexcol==False`, no index col will be used
      and a rangeindex will be created by pandas
    * if useindexcol is an int, then the Nth column will be
      used as the index column (0=1st column...)
    * `useindexcol` cannot be `True`, it is either `False` or `int`
    * `False` must be passed explicitly, not as `0`

    :param infile: the path to the input file
    :param column_names: custom names for our headers
    :param useindexcol: use an index column from the dataframe.
                        * `False` for no index
                        * `int` to select a column by its number
    :return: df, an initialized dataframe
    """
    if not ( isinstance(useindexcol, int)
             or isinstance(useindexcol, bool)
    ):
        raise TypeError(f"`useindexcol` should be of type `bool` or `int`, got `{useindexcol}`")
    elif ( (isinstance(useindexcol, bool) and useindexcol != False )
           or (isinstance(useindexcol, int) and useindexcol < 0)
    ):
        raise ValueError(f"`useindexcol` must be `False` or a null or positive integer, got `{useindexcol}`")
    return pd.read_csv(
        infile,
        sep="\t", quotechar='"', header=0, skip_blank_lines=True,
        names=column_names, index_col=useindexcol
    )


def write_df(df:DFType, outfile:str) -> None:
    """
    write a dataframe to a file

    :param df: the dataframe to write
    :param outfile: the path of the output file
    """
    df.to_csv( outfile, sep="\t", quotechar='"'
             , index_label="index", encoding="utf-8")
    return


def write_ro(ro:SType, outfile:str) -> None:
    """
    append `ro`, a single csv row represented as a pd.Series,
    to `outfile`. the file doesn't yet exist, append the header too.

    :param ro: the row to append
    :param outfile: the full path to the output file.
    """
    ro = pd.DataFrame([ro])             # type: ignore ;
    if not os.path.isfile(outfile):
        mode="w"
        header=True
    else:
        mode="a"
        header=False
    ro.to_csv(
        outfile, sep="\t", quotechar='"'
        , index_label="index", encoding="utf-8"
        , header=header, mode=mode
    )
    return


def copy_resource(innames:t.List[str], indir:str, outdir:str) -> t.List[str]:
    """
    copy an image file stored locally in `indir` to `outdir`.
    in the process, rename the file: replace the name by an UUID.

    :param innames : the array of input filenames to copy (without filepath)
    :param indir   : input directory
    :param outdir  : output directory
    :returns       : outnames, an array of output filenames.
    """
    outnames = []
    for i in innames:
        extension = re.search("\..+?$", i)[0]          # type: ignore ;
        outname = f"{build_uuid()}{extension}"
        shutil.copy2( os.path.join(indir, i)           # from
                    , os.path.join(outdir, outname) )  # to
        outnames.append(outname)
    return outnames


def write_log(idx:str, fname:str) -> None:
    """
    append a processed ID to a log file.
    * the log file has the following structure:
      `ID1|ID2|ID3`...
    * the indexes are the original dataset's IDs,
      not the UUIDs created when processing the
      rows.
    """
    with open(os.path.join(LOGS, fname), mode="a") as fh:
        fh.write(f"{idx}|")
    return

def make_dirs() -> None:
    """
    create all necessary directories to run the process.
    """
    if ( not os.path.isdir(TMP)
         or not os.path.isdir(os.path.join(TMP, "iconography")) ):
        os.makedirs(os.path.join(TMP, "iconography"))
    if ( not os.path.isdir(TMP)
         or not os.path.isdir(os.path.join(TMP, "cartography")) ):
        os.makedirs(os.path.join(TMP, "cartography"))
    if ( not os.path.isdir(OUT)
         or not os.path.isdir(OUT_ICONOGRAPHY)
         or not os.path.isdir(os.path.join(OUT_ICONOGRAPHY, "images")) ):
        os.makedirs(os.path.join(OUT_ICONOGRAPHY, "images"))
    if ( not os.path.isdir(OUT_CARTOGRAPHY)
         or not os.path.isdir(os.path.join(OUT_CARTOGRAPHY, "raster"))
         or not os.path.isdir(os.path.join(OUT_CARTOGRAPHY, "vector")) ):
        os.makedirs(os.path.join(OUT_CARTOGRAPHY, "raster"))
        os.makedirs(os.path.join(OUT_CARTOGRAPHY, "vector"))
    if not os.path.isdir(OUT_VERIFIER):
        os.makedirs(OUT_VERIFIER)
    else:
        for _ in os.listdir(OUT_VERIFIER):
            os.remove(os.path.join(OUT_VERIFIER, _))  # empty OUT_VERIFIER and delete it
        os.rmdir(OUT_VERIFIER)
        os.makedirs(OUT_VERIFIER)
    if not os.path.isdir(OUT_PLACE):
        os.makedirs(OUT_PLACE)
    if not os.path.isdir(LOGS):
        os.makedirs(LOGS)
    if not os.path.isdir(CLEANUP):
        os.makedirs(CLEANUP)
    if not os.path.isdir(os.path.join(CLEANUP, "iconography")):
        os.makedirs(os.path.join(CLEANUP, "iconography"))
    if not os.path.isdir(os.path.join(CLEANUP, "cartography")):
        os.makedirs(os.path.join(CLEANUP, "cartography"))
    if not os.path.isdir(os.path.join(CLEANUP, "place")):
        os.makedirs(os.path.join(CLEANUP, "place"))
    if not os.path.isdir(IN_TO_COMPLETE):
        os.makedirs(IN_TO_COMPLETE)
    if not os.path.isdir(OUT_TO_COMPLETE):
        os.makedirs(OUT_TO_COMPLETE)
    return


def deltmp(_dir:str=TMP) -> None:
    """
    delete all files/directories in the temp
    directory + the dir itself, recursively.

    :param _dir: the directory to delete. this allows to use the function recursively
    """
    try:
        # check that the directory is in `TMP` to avoid deleting everything
        if not ( os.path.commonpath([ os.path.abspath(TMP) ])
                 == os.path.commonpath([ os.path.abspath(TMP), os.path.abspath(_dir) ])
        ):
            print(f"directory `{_dir}` not in TMP. not deleting the files and exiting")
            sys.exit(1)

        # delete recursively
        for root, dirs, files in os.walk(_dir):
            for f in files:
                os.remove(os.path.join(root, f))
            for d in dirs:
                deltmp(os.path.join(root, d))
                # os.rmdir(os.path.join(root, d))

        # once you've deleted all its contents, delete itself `_dir`
        os.rmdir(_dir)

    # pass if `_dir` is aldready deleted
    except FileNotFoundError:
        pass
    return


"""
def delout(mode:t.Literal["icono","carto","place"]) -> None:
    " ""
    delete output directories and files that we need to delete.

    since this step is run in 3 steps (`-i`, `-c` `-l`), we only
    delete the output files relevant to each step (if `mode=="icono"`,
    we only delete the files in `out/iconography`).

    not implemented with step `-l`, since there's no need to delete
    anything there.

    we only delete image (vector/raster) files, not the csv datasets,
    which are overwritten at each step

    :param mode: icono, carto or place, depending on the step we're running
    "" "
    if mode == "place":
        return

    # open logs to see which files should be kept in `out/iconography/images`
    # ATTENTION AUX PROBLÈMES:
    # * actuellement, seulement les fichiers dans `log_iconography.json`
    #   ne sont pas supprimés, ce qui est cool mais pas assez: les fichiers
    #   créés avec `missing.csv` ne seront pas sauvegardés
    # * il y a eu un bug mal compris avec cette fonction qui supprime bcp
    #   trop d'images
    # * il faut intégrer `log_iconography_filestokeep.txt`, qui est plus safe
    #   mais seulement créé à la fin de `iconography.py`
    # => une solution possible: ne pas supprimer les fichiers image au début,
    #    mais seulement après avoir fini de run `iconography.py` ????????????
    #    bien réfléchir au stack pour pas tout sabrer (...)
    #    on pourrait tout garder en sortie par défaut, mais ne supprimer que ce
    #    qui n'est pas référencé comme image dans le tableur à la fin de
    #    `iconography.py`, ce qui faciliterait les choses

    tokeep = []  # files to keep
    try:
        with open(os.path.join(LOGS, "log_iconography.json"), mode="r") as fh:
            js = json.load(fh)
        for v in js.values():
            for _ in v[1]:
                tokeep.append(_)
        tokeep = list(set(tokeep))
    # no logfile yet
    except FileNotFoundError:
        pass

    out_carto_raster = os.path.join(OUT_CARTOGRAPHY, "raster")
    out_carto_vector = os.path.join(OUT_CARTOGRAPHY, "vector")
    out_icono_images = os.path.join(OUT_ICONOGRAPHY, "images")

    def deldir(_dir:str, _tokeep:t.List=[]):
        " ""
        delete all files in `_dir` directory recursively,
        excluding the files in `_tokeep`.
        :param _dir: absolute filepath to the directory
        :param _tokeep: array of filenames (without path) that shouldn't be deleted
        " ""
        for root, dirs, files in os.walk(_dir):
            for f in files:
                if f not in _tokeep:
                    os.remove(os.path.join(root, f))
            for d in dirs:
                if d not in _tokeep:
                    deldir(os.path.join(root, d))
                    os.rmdir(os.path.join(root, d))
        return

    if mode == "icono":
        deldir(out_icono_images, tokeep)
    elif mode == "carto":
        deldir(out_carto_raster, tokeep)
        deldir(out_carto_vector, tokeep)
    return
"""





