import pandas as pd
import re

from .constants import MUSEUM_API


# ********************************************
# complex conditions and value checking
# ********************************************


def isempty(val: str) -> bool:
    """
    check if a value is empty

    :param val: the value to check
    """
    empty = pd.isnull(val) or pd.isna(val)  # empty according to pandas
    empty_match = re.search("^(\s|\n)*$", val) if not empty else True  # actually empty string
    return empty and empty_match


def hasapi(val: str) -> bool:
    """
    check if `val` (a museum code) has an API
    """
    return val in MUSEUM_API.keys() and MUSEUM_API[val]

