import typing as t
import requests
import json
import time
import re


# **********************************************
# everything related to HTTP requests and API
# extraction (including IIIF API).
# **********************************************


def iiif_extract( iiif_url:str
                , folio:t.List[int]=[] ) -> t.List[ t.Optional[t.List[str]] ]:
    """
    using a valid IIIF manifest url,
    * request the manifest
    * check that it can be processed (it only contains a single
      image)
    * extract: a full size image URL and an image file format
    * return

    we only process manifests that contain a single image: it
    is the only way that we can determine which image to download.
    if a manifest contains 10 images but our row only describes 1
    image, downloading all 10 images adds noise to our carefully
    selected dataset.

    see:
    * https://api.bnf.fr/api-iiif-de-recuperation-des-images-de-gallica
    * https://iiif.io/api/presentation/3.0/
    * https://iiif.io/api/image/3.0/

    :param iiif_url: the url of the manifest to process
                         (has been validated by `regex.iiif_url_match`)
    :returns: [ [<url_img>, <fmt_img>], [<url_img>, <fmt_img>]... ]
              * url_img: the url for the full size image,
              * fmt_img: the format of the images
    """
    out = []
    r = requests.get(iiif_url)

    try:
        iiif_manifest = r.json()
    except json.JSONDecodeError:
        # something other than json was returned => print output and finish function here
        print(r.text)
        return out

    # array of IIIF canvases
    canvases = iiif_manifest["sequences"][0]["canvases"]
    # x = position of the requested canvas in `canvases` array of the manifest (given by `folio`)
    # returns: a ressource
    folio2resource = lambda x: (iiif_manifest["sequences"][0]
                                             ["canvases"][x]
                                             ["images"][0]
                                             ["resource"])
    # x = a IIIF ressource
    # returns: [ <image url>, <extension (extracted from the mimetype)> ]
    resource2img = lambda x: [ x["@id"]
                             , re.sub("^.+?/", "", x["format"])
                                 .replace("jpeg", "jpg") ]

    if len(folio):            # we have specified specific canvases to process
        for f in folio:
            resource = folio2resource(f)
            out.append(resource2img(resource))
    elif len(canvases) == 1:  # only 1 image => extract it
        resource = folio2resource(0)
        out.append(resource2img(resource))
    elif len(canvases) > 1:   # many resources in the manifest but no folio info given => can't decide which one to pick => don't extract anything
        pass

    return out






