from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from unidecode import unidecode
from glob import glob
import pandas as pd
import numpy as np
import typing as t
import roman
import uuid
import sys
import re
import os

from .constants import IN_ICONOGRAPHY, OUT
from .conditions import isempty


# ******************************************
# string  operations + regular expressions
# and regex based functions
#
# regex special characters: ".\\[{()*+?^$|"
#
# ******************************************


def splitcell(df:DFType, cols:t.List, retype:t.Dict={}) -> DFType:
    """
    split pipe-separated cells into lists (we also split by line jumps)

    :param df     : the dataframe to perform splits on
    :param cols   : the columns in which to split
    :param retype : a dict of columns to retype { <colname>: <type to retype to> }
    """
    # avoid type errors when columns contain only `np.nan` (and are float types)
    df[cols] = df[cols].fillna("")
    # split to list
    df[cols] = df[cols].apply( lambda x: x[cols]
                                          .astype(str)
                                          .str.split(r"[\|\n]", regex=True)
                             , axis=1)
    # remove empty list items retype pd.nan to `[]`
    for c in cols:
        df[c] = df[c].apply(lambda x: [ _.strip() for _ in x
                                        if isinstance(_,str) and len(_.strip()) ]  # `and len(_.strip())` removes all empty strings of `x`
                                        if isinstance(x,list) else [])
    # retype columns that should not contain string lists to their proper type
    if len( list(retype.keys()) ):
        for k,v in retype.items():
            if v == "int":
                df[k] = df[k].apply(lambda x: [ int(float(_)) for _ in x ])   # list of strings to list of ints
            else:
                raise ValueError (f"{v} not in defined retypes !")
    return df


def build_uuid() -> str:
    """
    create a UUID tailored to our database:
    a 32 character hexadecimal string (without the `-`)
    and prefixed by `qr1`
    these will be used to generate arks identifiers in Agorha.

    there are many ways to generate UUIDs. for simplicity, we
    use `uuid.uuid4()`, which just outputs a random sequence
    of characters.
    """
    return f"qr1{uuid.uuid4().hex}"


def simplify(val:str) -> str:
    """
    simplify a string: normalize accents and space, strip, convert to lowercase
    """
    return re.sub("\s+", " ", unidecode( val.lower() )).strip()


def idx_simple_match(idx:str) -> str:
    """
    perform a simple match on a row's id:
    a simple match is an id composed of:
    `^{museum_code}{separator}?{hexadecimal_id}{separator}?{subid}?{separator}?$`
    1) museum_code: `MC`, `BNF` (compulsory)
    2) hexadecimal_id: `112`, `A9` (compulsory)
    3) subidentifier: `1`, `19` (ex: the last "1" in MC_112-1) (optionnal)
    4) separator: `-`, `\s`, `_` (optionnal): separators between
       =/= parts of `idx`.

    the regex used is kind of ugly to allow better group capture.

    :matched:
    MC112
    MC_112
    MC 112-
    MC_112-1
    :not_matched:
    MC
    mc112
    mc@112
    :subgroups: based on the example "MC 112-1"
    1) museum code:    "MC"
    2) hexadecimal id  "112"
    3) subidentifier   "1"

    :param idx: the identifier on which we'll be performing a match
    :returns: * if there's a match, a normalized index
              * if there's no match, an empty string
    """
    idx_norm = ""
    if not (isempty(idx)):
        idx = idx.strip()
        simple_match = re.match(r"^([A-Z]+)[\s_-]*([A-Z\d]+)[\s_-]*(\d)*[\s_-]*$", idx)
        idx_norm = idx_normalise(simple_match) if simple_match else ""
    return idx_norm


def idx_complex_match(idx: str) -> t.List[str]:
    """
    perform a complex match. this one is more complicated than
    a simple match (and it is not, in fact, a match).
    we have two types of complex ids: pipe-separated (`BNF 1 | BNF 2`)
    ones and "composite" ones (`BNF 1-1 à 5`).

    1.pipe separated ids are simply split into lists
    2.for complex matches:
      * we match `{museum_id}{separator}?{hexadecimal_id}` (`BNF 258`)
        to be sure that we're working with a valid row id.
      * we extract all other digits after the above pattern,
        which are interpreted as subindexes.
      * from that, we see how many images are contained within
        this complex id (2 for `BNF 258-1 et 2`, 4 for `BNF 258-1 à 4`
        and we decompose the complex array into an array of
        normalized image ids (`["BNF_258-1, "BNF_258-2", "BNF_258-3"]`)

    :param idx: the index to process
    :returns: * if we have a complex match: an array of rebuilt indexes.
              * else: an empty array
    """
    idx_norm = []
    if "|" in idx:
        idx_norm = [ _.strip() for _ in idx.split("|")
                     if not re.search(r"^[\n\s]*$", _) ]
    else:
        base_match = re.search(r"^([A-Z]+)[\s_-]*([A-Z]*\d+)", idx)
        if base_match:
            _idx = idx
            idx = idx.replace(base_match[0], "")                     # only keep the non-matched part of `idx`
            subindexes = [ int(i) for i in re.findall("\d+", idx) ]  # multiple numberic sub-indexes
            # `et` => only keep the mentionned subindexes
            if len(subindexes) > 0 and re.search(r"\set\s", _idx):
                for s in subindexes:
                    idx_norm.append(f"{base_match[1]}_{base_match[2]}-{s}")
            # for other cases, create a range
            elif len(subindexes) > 0:
                for s in range( min(subindexes), max(subindexes)+1 ):
                    idx_norm.append( f"{base_match[1]}_{base_match[2]}-{s}" )
    return idx_norm


def idx_normalise(idx_match:re.Match) -> str:
    """
    normalise the a simple index based on the index's
    match object. only useful with simple matches

    :param idx_match: the index matched
    :param simple: a flag indicating wether the index is simple or complex
    :returns: the normalized index
    """
    subidx = f"-{idx_match[3]}" if idx_match[3] is not None else ""
    idx = f"{idx_match[1]}_{idx_match[2]}{subidx}".lower()
    return idx


def iiif_url_match(idx:str, iiif_url:str) -> str:
    """
    match and extract a IIIF URL based on institution urls.

    :param idx: the current row's index
    :param iiif_url: the url to the IIIF manifest to validate
    :returns: the IIIF url if it is valid. else, an empty string
    """
    matched = False
    if not isempty(iiif_url):
        # PATTERNS MUST BE UPDATED TO FIT NEW VALID
        # URL PATTERNS BY OTHER INSTITUTIONS
        patterns = [
            "https://apicollections\.parismusees\.paris\.fr/iiif/\d+/manifest"
            , "https://gallica\.bnf\.fr/iiif/ark:/12148/.+/manifest\.json"
        ]
        for p in patterns:
            iiif_match = re.search(p, iiif_url)
            if iiif_match:
                # extract the url from the row
                iiif_url = iiif_match[0]
                matched = True
                break

            elif "gallica.bnf.fr/view3if" in iiif_url:
                # an link to the IIIF viewer has been added
                # , not to the IIIF manifest. rebuild the
                # link to the IIIF manifest.
                view3if_match = re.search("https://gallica.bnf.fr/view3if/ga/ark:/12148/(.+)", iiif_url)
                if view3if_match:
                    iiif_url = f"https://gallica.bnf.fr/iiif/ark:/12148/{view3if_match[1]}/manifest.json"
                    matched = True
                    break

    # delete the iiif url if it isn't valid. return the result
    if matched == False:
        print(f"invalid IIIF URL on row {idx}: {iiif_url}") if not isempty(iiif_url) else ""
        iiif_url = np.nan
    return iiif_url


def idx2filename(idx:str) -> t.List[str]:
    """
    match an image file based on the csv row's index `idx`
    :param idx: the index we're working on
    :returns: the list of matched filenames
    """
    filenames = []
    images_in = glob(os.path.join(IN_ICONOGRAPHY, "images", "*"))  # full path for all images

    for i in images_in:
        fname = os.path.basename(i)  # filename without path
        id_img = re.sub("\..*?$", "", os.path.basename(i)).lower()  # filename without path and extension

        if idx == id_img.lower() \
        or re.search(f"^{idx}[^a-zA-Z0-9]+$", id_img.lower()) \
        or re.sub(f"_\s-", " ", idx.lower()) == re.sub(f"_\s-", " ", id_img.lower()):
                # perfect match between filename and index
                # or near perfect match (i.e., the filename contains
                # irrelevent characters at the end, or the filename
                # matches `idx` without taking separators intro account)
                filenames.append(fname)
    return filenames


def date2range(date:str) -> t.List[int]:
    """
    transform "human readable" dates into a range
    of 2 extreme dates in `[YYYY,YYYY]` format.

    if the date is exact ("En 1840"), we build a range
    containing twice the same date (`[1840,1840]`).

    :param date: the date string we're working on
    :returns: `[YYYY,YYYY]` if there's a date
              , `[]` if no date
    """
    date_array = []
    if not isempty(date):
        date_match = re.findall("\d{4}", date)  # `YYYY`
        vers_match = re.search("[Vv]ers.*?(\d{4})", date)  # `Vers 1821`
        avap_match = re.search("([Aa]vant|[Aa]près).*?(\d{4})", date)  # `Avant YYYY`,`Après YYYY`
        approx_match = re.search("\d{2}[\d\.\?]{2}", date)  # `YY..`, `YY??`, `YYY.`
        century_match = re.search("(\d{2})(e|è|eme|ème|th).*?(siècle|century|s|c)", date)  # `19e siècle`, `19 s.`
        century_roman_match = re.findall("(?<=[\s,\.\-\:\;])[CLMXVI]+(?=[\s,\.\-\:\;])", date)  # contains roman numbers => roman century

        # 1) Before/After YYYY dates
        if avap_match:
            if re.search("[Aa]vant", avap_match[1]):
                # before YYYY => round to the start of the century: `Avant 1830` => `[1801, 1830]`
                date_array = [ int(re.sub("\d{2}$", "01", avap_match[2])), avap_match[2] ]
            else:
                # after YYYY => round to the end of the century: `Après 1830` => `[1830, 1900]`
                d = int(re.search("^\d{2}", avap_match[2])[0])  # first 2 digits
                date_array = [ int(avap_match[2]), int(f"{str(d+1)}00") ]

        # 2) `Vers YYYY` dates => +/-10 years
        elif vers_match:
            date_array = [ int(vers_match[1])-10, int(vers_match[1])+10 ]


        # 3) simple dates in YYYY format.
        elif len(date_match) >= 1:
            # 1+ year(s) were extracted => build a range
            date_array = [int(min(date_match)), int(max(date_match))]

        # 4) incomplete date: 18.., 182., 182? ... => round it to the decade/century
        elif approx_match:
            num = re.search("\d+", approx_match[0])[0]     # identified numbers in the date: `18`
            nan = re.search("[^\d]+", approx_match[0])[0]  # unidentified numbers: `..`
            roof = int(num)+1  # upper decade/century than the one pointed by the numbers in `date`: `18` => `19`
            zeroes = re.sub(".", "0", nan)  # numbers to add at the end of `num` to have a valid YYYY date
            date_min = int(num + zeroes) + 1
            date_max = int(str(roof) + zeroes)
            date_array = [ date_min, date_max ]

        # 5) century in arabic or roman numerals
        elif century_match or century_roman_match:
            # first 2 digits of the date range, converting
            # roman numbers to arabic if necessary
            if len(century_roman_match) > 1:  # 2 centuries in roman numbers
                try:
                    floor_century, roof_century = sorted([ roman.fromRoman(c)
                                                           for c in century_roman_match ])
                except roman.InvalidRomanNumeralError:
                    print("roman number error: \n*", century_roman_match, "\n*", date)
                    sys.exit()
                floor_century = floor_century-1  # floor is the start of the first century named: `XVIII`: `17`, for `1700`
            elif len(century_roman_match) == 1:  # 1 century in roman numbers
                roof_century = roman.fromRoman(century_roman_match[0])  # `XVIII` => `18` is the roof
                floor_century = str( int(roof_century)-1 )              # `XVIII` => `17` is the floor
            else:
                roof_century = century_match[1]             # `18e siècle` => `19`
                floor_century = str( int(roof_century)-1 )  # `18e siècle` => `19`

            # special cases
            part = re.search("([1-4])e.*?(moiti[ée]|quart)", date)   # 1ère moitié...
            aprx = re.search("([Dd][ée]but|[Mm]ilieu|[Ff]in)", date)  # `Début XIXe siècle`

            # try to target part of a century: half or part
            if part:
                # arrays to build the last 2 digits
                moitie = [ ["01", "50"], ["51", "00"] ]
                quart = [ ["01", "25"], ["26", "50"], ["51", "75"], ["76", "00"] ]
                target = int(part[1])-1  # index of the element to target in `moitie` or `quart`
                base = [ floor_century, floor_century ]  # to change the first 2 digits in date range if needed

                # extract the last 2 digits
                if part[2] == "quart":
                    # switch the first 2 digits of the roof date to the upper century
                    if quart.index(quart[target]) == len(quart)-1:
                        base = [ floor_century, roof_century ]
                    floor = quart[target][0]
                    roof = quart[target][1]
                else:
                    # switch the first 2 digits of the roof date to the upper century
                    if moitie.index(moitie[target]) == len(moitie)-1:
                        base = [ floor_century, roof_century ]
                    floor = moitie[target][0]
                    roof = moitie[target][1]
                date_array = [ int(f"{base[0]}{floor}"), int(f"{base[1]}{roof}") ]

            # debut/fin/milieu
            elif aprx:
                deb = [ "01", "25" ]
                mil = [ "25", "75" ]
                fin = [ "75", "00" ]
                if re.search("[Dd][ée]but", aprx[0]):
                    date_array = [ int(f"{floor_century}{deb[0]}"), int(f"{floor_century}{deb[1]}") ]
                elif re.search("[Mm]ilieu", aprx[0]):
                    date_array = [ int(f"{floor_century}{mil[0]}"), int(f"{floor_century}{mil[1]}") ]
                elif re.search("[Ff]in", aprx[0]):
                    date_array = [ int(f"{floor_century}{fin[0]}"), int(f"{roof_century}{fin[1]}") ]

            # target whole century
            else:
                date_array = [ int(f"{floor_century}01"), int(f"{roof_century}00") ]

    # if `date` doesn't only contain dates, errors occurs (fake
    # matching of roman centuries) => only keep dates that make sense
    date_array = [ int(_) for _ in date_array ]
    if len(date_array) > 0 and ( min(date_array)<1000 or max(date_array)>2100 ):
        date_array = []

    return date_array



