# `1_data_preparation`

Ce dépôt contient la seconde étape de la chaîne de traitement du
programme Richelieu. Histoire du quartier: nettoyage des jeux de
données. Cette étape est "semi-automatique": des tableurs doivent
être complétés à la main avant de relancer l'étape une deuxième 
fois pour intégrer les données produites manuellement.

---

## INSTALLATION ET UTILISATION (GNU/Linux + MacOS)

En supposant que l'on a tous les fichiers en entrée au bon endroit (voir plus bas).
Il faut installer GDAL, ce qui est fait au niveau de l'OS et pas du virtualenv. Pour
l'installer sur Ubuntu, un script a été créé. Sinon, voir [la doc officielle](https://gdal.org/download.html).

Cette étape de de la pipeline doit être **relancée deux fois**, une première fois pour tout traiter,
une seconde fois après avoir complété un fichier `missing.csv` qui permet de pointer vers les images
manquantes.

Les fichiers d'entrée (Iconographie, Localisation et Cartographie) peuvent être nommés différemment,
mais dans ce cas il faut faire attention à reporter les changements de nom dans le code.

```bash
# install the GDAL library. 
# !!!! impacts the whole OS, not just the venv !!!!
sudo bash ./scripts/gdal_install.sh 

# download the repo
git clone https://gitlab.inha.fr/pkervega/rich.data_data_preparation.git  # clone the repo locally
cd 1_data_preparation                                                     # go in the project folder
# copy the input files in `in/`. see below

# virtual env + dependencies
python3 -m venv env                # create a virtual env
source env/bin/activate            # source the env
pip install -r requirements.txt    # install other dependencies

# usage
python main.py
# after using it once, move `src/logs/to_complete_out/missing.csv` to
# `src/logs/to_complete_in/missing.csv` and complete it
python main.py  # integrate the inputs in `src/logs/to_complete_in/`
```

---

## PRÉPARATION DES DONNÉES

### Avant utilisation

Ajouter les données:

- **Iconographie**: 
  - **dans `in/iconography`**, il faut avoir 
    - le fichier CSV à traiter (plus bas nommé `iconography.csv`, même si son nom peut être différent)
    - tous les fichiers images de toutes les entrées du tableur, à télécharger sur le Cloud, avec les
      espaces des noms de fichier remplacés par des `_`.
  - **le plus simple**, c'est de télécharger tous les dossiers contenant des images du cloud, de les mettre
    dans un dossier et d'utiliser **[./scripts/prepare_icono_dir.py](prepare_icono_dir.py)**, qui fait tout
    le travail de nettoyage et de déplacement des fichiers. ensuite, déplacer l'output de ce script dans 
    `in/iconography/images`.
    - *en détail, il faut*:
      - *télécharger les fichiers image localement (dans `in/iconography/images`). Tous les fichiers
        doivent être à la racine du dossier `images/`.*
      - *normaliser la manière dont ils sont nommées en remplaçant les espaces par des `_`.*
  - **ajouter `missing.csv`**, un fichier produit à cette étape, dans `src/logs/to_complete_in/` 
    et le compléter (voir plus bas)
- **Cartographie**: 
  - **télécharger les fichiers raster** dans `in/cartography/raster`. Dans le projet QGis, les
    *GeoTiff* à utiliser se trouvent dans le dossier `Raster/`
  - **télécharger les fichiers vecteur** dans `in/cartography/vector`. Dans le projet QGis, les 
    *shapefile* à utiliser se trouvent tous dans le dossier `Vecteur/INHA/_Definitif/`.
  - **ajouter le CSV de métadonnées**, disponible à  [cette adresse](https://cloud.inha.fr/f/770664) 
    et le **pré-traiter**: 
    - on supprime les lignes "d'en-tête", qui séparent les différentes parties du corpus (une ligne
      par institution, avec en général, sur la 2e colonne, le nom de l'institution dont le corpus va suivre).
    - on supprime les colonnes vides ou avec du texte mais pas d'en-tête à la fin
- **Localisations**:
  - concaténer les différentes feuilles de localisation associées aux tableurs d'iconographie
  - supprimer les dernières colonnes, du tableur sans titre

Dans les dossiers `raster`, `vector`, `images`, tous les fichiers doivent être à la racine du dossier.
On doit donc avoir la structure suivante dans le dossier `in/` (les noms de fichier peuvent changer,
adapter le code si besoin):

```
in/
 |__location/           : dossier pour le traitement de la feuille de localisations
 |   |__location.csv    : tableur de localisations
 |__iconography/        : dossier pour le traitement du tableur et des données iconographiques
 |   |__iconography.csv : le tableur iconographie
 |   |__images/         : les fichiers image
 |__cartography/        : dossier pour le traitement du tableur et des données cartographiques
     |__cartography.csv : le tableur de métadonnées
     |__raster/         : les fichiers raster utilisés
     |__vector/         : les fichiers vecteur utilisés
```

---

### Après une première utilisation: correction manuelle

À cette étape, le tableur `missing.csv` (entrées du tableur iconographique 
qui n'ont pas été rattachées à des images) est enregistré dans `src/logs/to_complete_out/`.
Il faut le copier dans `src/logs/to_complete_in/`, le remplir en complétant la colonne 
`filenames` par un ou plusieurs noms de fichiers. 

- les fichiers correspondants doivent être présents dans `in/iconography/images/`.
- si il y a plusieurs fichiers, les séparer par des `|`) 

**Après avoir rempli le tableur, relancer le programme: `python main.py` pour intégrer les 
images ajoutées**.

---

## LOGS

Certaines étapes peuvent prendre très longtemps ou demander une complétion manuelle. C'est 
pourquoi on utilise plusieurs fichiers de log, en CSV ou JSON, qui permettent de sauvegarder
des données d'une utilisation à l'autre.

**NE PAS LES SUPPRIMER, SAUF SI ON VEUT AVOIR À TOUT RECOMMENCER**

```
src/logs/
├── log_iconography_compressthumbnail.json : fichiers icono compressés et redimensionnés
├── log_iconography_outfile.json           : fichier icono téléchargés
├── to_complete_in/                        : fichiers complétés manuellement utilisés en entrée de cette étape
│   └── missing.csv                        : csv listant les entrées du tableur icono sans images associées (après complétion manuelle)
└── to_complete_out/                       : fichiers à compléter manuellement produits à cette étape
    └── missing.csv                        : entrées du tableur icono sans images rattachées, à compléter manuellement.
```

---

## REMARQUES ET PRINCIPES
  
- **les 4 grandes étapes**: chacune correspond à un fichier dans `./src/*py`:
  - `verifier.py`: on vérifie la qualité des données avant de se lancer dans
    leur traitement. À minima, on s'assure que les jointures entre localisation,
    icono et carto pourront être réalisées.
  - `iconography.py`: on traite le fichier d'icono. Le plus difficile est d'associer
    chaque ligne à un fichier image: les noms de fichier dans `in/iconography/images`
    sont dérivés de "l'identifiant interne" de chaque ligne, mais des erreurs arrivent.
    Dans l'idéal, on utilise le IIIF pour retélécharger les images. À la fin de cette étape,
    il sera nécessaire de compléter le fichier `missing.csv` pour ajouter les noms de fichier
    qui n'ont pas pu être récupérés automatiquement.
  - `cartography.py`: on traite les *shapefiles* et le tableur contenant des métadonnées
    sur les parcelles à la feuille. Il s'agit de mettre bout à bout tous les *shapefiles*
    dans une seule GeoDataFrame, puis de la convertir en DataFrame pure. 
    - les vecteurs et rasters sont reprojetés, et les fichiers `GeoTiff` sont convertis en PNG.
    - les données vectorielles sont converties du `shapefile` au `geojson`, et sauvegardées
      dans une colonne `vector` du tableur produit en sortie. Un `geojson` entier n'est
      pas produit, mais seulement une `geometry`: il devra être inclu à une `featurecollection`
      *on the fly* avant d'être envoyé au frontend.
  - `location.py`: la table de localisations est plutôt propre et ne demande qu'un léger nettoyage.
- **Système de coordonnées**: toutes les données cartographiques sont harmonisées en
  **EPSG:3857 / WGS84**, système de coordonnées cartésien utilisé par Leaflet.
- **UUIDs**: des UUIDs prefixés par `qr1` sont utilisés comme indexes de toutes les données
  de tous nos tableurs, et comme noms pour les fichiers images dans `out/`.
  - les fichiers cartographiques sont suffixés par `_epsg_3857.<extension>`, pour garder 
    l'info sur le système de projection utilisé.
- **`cleanup/`**: à chaque fois que le programme est lancé, des fichiers image sont déplacés
  dans `out/` et renommés par un UUID. Dans `out/`, il y a donc une multiplication de 
  fichiers inutiles (liés à une précédente exécution). Ces fichiers sont déplacés dans
  le dossier `./cleanup/`, où ils peuvent être récupérés en cas d'erreur.

---

## LICENCE ET CRÉDITS

Code sous licence GNU GPL 3.0, données sous licence CC BY 4.0.
