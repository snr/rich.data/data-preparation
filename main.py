import argparse

from src.place import Place
from src.iconography import Iconography
from src.cartography import Cartography
from src.verifier import Verifier
from src.utils.io import make_dirs, deltmp


# *****************************************************
# command line interface to launch the pipelines
# *****************************************************


if __name__ == "__main__":
    # delete output dirs + recreate them
    """
    delout("icono")
    delout("carto")
    delout("location")
    """
    make_dirs()

    # run
    print("running basic verifications before cleaning")
    print("*******************************************")
    Verifier().pipeline()

    print("\nrunning the dataset cleaning pipeline")
    print("*************************************")

    print("\nprocessing the cartography dataset\n"
          "**********************************")
    Cartography().pipeline()

    print("\nprocessing the iconography dataset\n"
          "**********************************")
    Iconography().pipeline()

    print("\nprocessing the place dataset\n"
          "*******************************")
    Place().pipeline()

    deltmp()
    print("\n* dataset cleaning done ! \n"
          + "* if this is the first time you use the script, copy `actors.csv` \n"
          + "  and `missing.csv` to `in/iconography` and relaunch this step.\n"
          + "* else, copy `out/*` to `2_data2sql/in` to continue the pipeline.\n")


